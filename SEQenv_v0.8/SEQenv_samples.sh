#!/bin/bash
# ***************************************************************
# Name:    SEQenv_samples.sh
# Purpose: Pipeline to link sequences to environmental descriptive terms when species abundance file is given
# Dependencies:  
#       Rscripts
#               Install in R:     
#                       install.packages("optparse")
#                       install.packages("wordcloud")
#                       install.packages("RColorBrewer")
#                       install.packages("optparse")
#                       install.packages("gplots")
#       Perlscripts
#               Package: ONTO-PERL
#               Install: perl ‐MCPAN -e shell install Bundle::CPAN
#                                             install OBO::Core::Ontology
#       Graphviz
#               Install: http://www.graphviz.org/Download.php
#       GNU Parallel 
#               Install: http://www.gnu.org/software/parallel/
#       blastn/blastp
#               Software: http://www.ncbi.nlm.nih.gov/books/NBK1763/
#               nt/nr database, go to your local blastn db folder and use update_blastdb.pl: 
#                       update_blastdb.pl --showall
#                       update_blastdb.pl --decompress nt/nr
#       SEQenv_tagger
#               Software is provided in the installation folder
#               Run "make" to compile it
#               Prerequisite: boost library along with it's developmental version
#               For example, in CentOS, you can run the following command:
#                       sudo yum install boost boost-devel 
# Version:   0.8
# Authors:   Umer Zeeshan Ijaz (Umer.Ijaz@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/umer.ijaz/index.htm
#            Evangelos Pafilis (vagpafilis@gmail.com)
#                  http://epafilis.info
# Acknowledgements:
#            This work is made possible through the following hackathons supported by European Union's 
#            Earth System Science and Environmental Management ES1103 COST Action ("Microbial ecology & 
#            the earth system: collaborating for insight and success with the new generation of sequencing tools"):
#                       (1) From Signals to Environmentally Tagged Sequences" (Ref: ECOST-MEETING-ES1103-050912-018418)
#                           September 27th-29th 2012, Hellenic Centre for Marine Research, Crete, Greece
#                       (2) From Signals to Environmentally Tagged Sequences II" (Ref: ECOST-MEETING-ES1103-100613-031037)
#                           June 10th-13th 2013, Hellenic Centre for Marine Research, Crete, Greece
#            This software is made possible through the useful discussions between the following participants of the above 
#            mentioned hackathons:
#                       Chris Quince (cq8u@udcf.gla.ac.uk) 
#                       Umer Zeeshan Ijaz (Umer.Ijaz@glasgow.ac.uk) 
#                       Evangelos Pafilis (pafilis@hcmr.gr) 
#                       Christina Pavloudi (cpavloud@hcmr.gr) 
#                       Anastasis Oulas (oulas@hcmr.gr) 
#                       Julia Schnetzer (jschnetz@mpi-bremen.de) 
#                       Aaron Weimann (aaron.weimann@uni-duesseldorf.de) 
#                       Alica Chronakova (alicach@upb.cas.cz) 
#                       Ali Zeeshan Ijaz  (alizeeshanijaz@gmail.com) 
#                       Simon Berger (simon.berger@h-its.org)                   
# Last modified:   2013-07-17
# License:   Copyright (c) 2013 Computational Microbial Genomics Group, University of Glasgow, UK
#
#            This program is free software: you can redistribute it and/or modify
#            it under the terms of the GNU General Public License as published by
#            the Free Software Foundation, either version 3 of the License, or
#            (at your option) any later version.
#
#            This program is distributed in the hope that it will be useful,
#            but WITHOUT ANY WARRANTY; without even the implied warranty of
#            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#            GNU General Public License for more details.
#
#            You should have received a copy of the GNU General Public License
#            along with this program.  If not, see <http://www.gnu.org/licenses/>.
# **************************************************************/     

HELPDOC=$( cat <<EOF
SEQenv -samples- Pipeline to link sequences to environmental descriptive terms when species abundance file is given

Usage:
    bash `basename $0` -f <fasta_file.fasta> -s <species_abundance_file.csv> [options]

Options:
    -t Text source (1: GenBank record "isolation source" field, 2: PubMed abstracts (Default: 1))

    -l Presence/absence flag for species abundance file 

    -p Parallelize using GNU Parallel flag
    -c Number of cores to use (Default: 10)

    -o Filtering method (1: -n most abundant OTUs, 2: minimum OTUs sum >= -n (Default: 1))
    -n Filtering threshold (Default: 1000)	

    -m Minimum percentage identity in blastn (Default: 97)
    -q Minimum query coverage in blastn (Default: 97)
    -r Number of reference matches (Default: 10)

    -d Extract terms for the given ENVO ID (Default: all)
        all=Consider all terms
	Examples:
	ENVO:00010483=Environmental Material	 		
	ENVO:00002297=Environmental Features
	ENVO:00000428=Biome
	ENVO:00002036=Habitat

EOF
)

set -o errexit

# = Parameters to set ============== #
LOGFILE="`pwd`/SEQenv.log" # Where to save the log
BLASTN_DIR="/home/opt/ncbi-blast-2.2.28+/bin"; # Path where blastn is installed
BLASTDB_DIR="/home/opt/ncbi-blast-2.2.28+/db"; # Path where nt is installed
FASTA_FILE=""   # This field should be empty
SPECIES_ABUNDANCE_FILE="" # This field should be empty

#default input arguments
PARALLELIZE_FLAG=0
PRESENCE_ABSENCE_FLAG=0
NUMBER_OF_CORES=10
NUMBER_OF_REFERENCE_MATCHES=100
MINIMUM_PERCENT_IDENT=97
MINIMUM_QUERY_COVERAGE=97
NUMBER_OF_FILTERED_OTUS=1000
ENVO_TERM="all"
FILTERING_METHOD=1
TEXT_SOURCE=1

#document matching and counting for envo score calcuation
DOCUMENT_MATCH="single" # Possible values:multiple,single
DOCUMENT_COUNT="aggr" # Possible values: aggr (aggregative), freq (document frequency weight applies) [default]
NORMALIZATION="divrowsum" # Possible values: none, zscore, divrowsum, zscore

# =/Parameters to set ============== #

CURRENT_DIR=`pwd`

# = Enable FP support ============== #
# By default, there is limited capability in bash to handle floating point
# operations. In this script bc is used to calculate the floating point operations.
# $float_scale parameter specifies the precision of the floating point.
# Reference: http://www.linuxjournal.com/content/floating-point-math-bash

float_scale=5
# Evaluate a floating point number expression.

function float_eval()
{
    local stat=0
    local result=0.0
    if [[ $# -gt 0 ]]; then
        result=$(echo "scale=$float_scale; $*" | bc -q 2>/dev/null)
        stat=$?
        if [[ $stat -eq 0  &&  -z "$result" ]]; then stat=1; fi
    fi
    echo $result
    return $stat
}


# Evaluate a floating point number conditional expression.

function float_cond()
{
    local cond=0
    if [[ $# -gt 0 ]]; then
        cond=$(echo "$*" | bc -q 2>/dev/null)
        if [[ -z "$cond" ]]; then cond=0; fi
        if [[ "$cond" != 0  &&  "$cond" != 1 ]]; then cond=0; fi
    fi
    local stat=$((cond == 0))
    return $stat
}

function ceil () {
  echo "define ceil (x) {if (x<0) {return x/1} \
        else {if (scale(x)==0) {return x} \
        else {return x/1 + 1 }}} ; ceil($1)" | bc;
 }

# =/Enable FP support ============== #


# Create directories if they don't exist yet
function create_dirs() {
    local dir
    for dir in "$@"
    do
        if [ ! -d "$dir" ]; then
            mkdir "$dir"
        fi
    done
}

# Check if files exist
function check_prog() {
    local prog
    for prog in "$@"
    do
    if which $prog >/dev/null; then
        SEQenv_print 'Using ' $prog
    else
        echo "$prog not in your path" >&2; exit 1;
    fi

    done
}

function skip_gen_file(){
    if [ -f "$1" ]; then
    echo "true"
    else
    echo "false"
    fi
}

function skip_gen_dir(){
    if [ -d "$1" ]; then
        echo "true"
    else
        echo "false"
    fi
}

function SEQenv_print() {
    echo [`date "+%Y-%m-%d %H:%M:%S"`] "$@" | tee -a $LOGFILE
}


# Parse options
while getopts ":plhc:r:n:f:s:m:q:d:t:o:" opt; do
    case $opt in
        p)
            PARALLELIZE_FLAG=1
            ;;
	l)
	    PRESENCE_ABSENCE_FLAG=1
	    ;;
        t)
	    TEXT_SOURCE=$OPTARG
	    ;;
        f)
            FASTA_FILE=$OPTARG
            ;;
	o)
	    FILTERING_METHOD=$OPTARG
	    ;;
        s)
            SPECIES_ABUNDANCE_FILE=$OPTARG
            ;;
        c)
            NUMBER_OF_CORES=$OPTARG
            ;;
	d)
	    ENVO_TERM=$OPTARG
	    ;;
        r)
            NUMBER_OF_REFERENCE_MATCHES=$OPTARG
            ;;
        m)
            MINIMUM_PERCENT_IDENT=$OPTARG
	    ;;
        q)
            MINIMUM_QUERY_COVERAGE=$OPTARG
	    ;;	
        n)
            NUMBER_OF_FILTERED_OTUS=$OPTARG
            ;;

        h)
            echo "$HELPDOC"
            exit 0
            ;;
        \?)
            echo "$HELPDOC"
            echo "Invalid option: -$OPTARG" >&2
            exit 1
            ;;
    esac
done
if [ -z $FASTA_FILE ] || [ -z $SPECIES_ABUNDANCE_FILE ]
then
        echo "$HELPDOC"
        exit 1
fi

if [ $TEXT_SOURCE -gt 2 ] || [ $TEXT_SOURCE -lt 1 ]
then
        echo "$HELPDOC"
        exit 1
fi

if [ $FILTERING_METHOD -gt 2 ] || [ $FILTERING_METHOD -lt 1 ]
then
        echo "$HELPDOC"
        exit 1
fi

SEQenv_print SEQenv -samples- v0.8
check_prog $BLASTN_DIR/blastn

if [ $PARALLELIZE_FLAG -eq 1 ]
then
    check_prog parallel
fi

# Using /usr/bin/dirname to get the full path to this script location
# without being affected from where this script was invoked
export SEQENV_PIPELINE_DIR=$(cd "$(dirname "$0")"; pwd)

check_prog $SEQENV_PIPELINE_DIR/SEQenv_tagger/seqenv

if [[ -n $SEQENV_PIPELINE_DIR ]];
then
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_parse_environments.sh
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_blast_concat_PMID3.py
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_get_linked_pmids.py
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_extract_ids_level.pl  
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_gen_word_cloud.R  
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo.obo
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_extract_records.pl    
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_gen_heapmap.R        
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_get_abstract.py   
    check_prog $SEQENV_PIPELINE_DIR/scripts/fastagrep.pl
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_blast_concat_isolation_terms.py  
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_filter_blast.pl       
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_gen_OTUs_freq.pl
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_gen_samples_freq.R
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_process_weight_matrix.R
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_extract_OTUs.R
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_gen_dot_files.pl      
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_ids_to_terms.pl
else
    SEQenv_print "SEQENV_PIPELINE_DIR is not set. Unable to locate scripts directory!"
    exit 1;
fi

fileName=`echo "$(basename $FASTA_FILE)" | cut -d'.' -f1`


# Filter out OTUs based on $FILTERING_METHOD
if [ $FILTERING_METHOD -eq 1 ]; then
	SEQenv_print "STEP 1: Get sequences for $NUMBER_OF_FILTERED_OTUS most abundant OTUs." 
else
        SEQenv_print "STEP 1: Get sequences where minimum OTUs sum >= $NUMBER_OF_FILTERED_OTUS."
fi
filteredOTUsFileName=$fileName'_N'$NUMBER_OF_FILTERED_OTUS
if [ "$(skip_gen_file $filteredOTUsFileName'.fa')" == "true" ];then
   SEQenv_print $filteredOTUsFileName'.fa' already exists! Skipping this step.
else
   Rscript $SEQENV_PIPELINE_DIR/scripts/envo_extract_OTUs.R -s $SPECIES_ABUNDANCE_FILE -m $FILTERING_METHOD -t $NUMBER_OF_FILTERED_OTUS -l OTU_list.txt
   uniqueIDs=$(cat OTU_list.txt)
   for id in ${uniqueIDs[@]}
   do
        perl $SEQENV_PIPELINE_DIR/scripts/fastagrep.pl -X "$id" $FASTA_FILE >> $filteredOTUsFileName'.fa'
   done
   rm OTU_list.txt
   SEQenv_print $filteredOTUsFileName'.fa' is successfully generated.
fi

# Run blastn on $filteredOTUsFileName'.fa'
SEQenv_print "STEP 2: Blast against NCBI's nt database with minimum percentage identity of $MINIMUM_PERCENT_IDENT%, maximum of $NUMBER_OF_REFERENCE_MATCHES reference sequences, and evalue of 0.0001 in blastn."
# Format for blastn
blastOutFmt="\"6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs staxids\""
blastFileName=$filteredOTUsFileName'_blast'
if [ "$(skip_gen_file $blastFileName'.out')" == "true" ];then
   SEQenv_print $blastFileName'.out' already exists! Skipping this step.
elif [ $PARALLELIZE_FLAG -eq 1 ]; then

    # Get the file size in KB
    sizeFileBytes=$(du -b ${filteredOTUsFileName}'.fa' | sed 's/\([0-9]*\)\(.*\)/\1/')
    sizeChunks=$(ceil $(float_eval "$sizeFileBytes / ($NUMBER_OF_CORES * 1024)"))
    sizeChunksString="${sizeChunks}k"
    startTime=`date +%s`

    cat $filteredOTUsFileName'.fa' | parallel --block $sizeChunksString --recstart '>' --pipe $BLASTN_DIR/blastn -perc_identity $MINIMUM_PERCENT_IDENT -evalue 0.00001 -dust no -num_threads 1 -outfmt $blastOutFmt -max_target_seqs $NUMBER_OF_REFERENCE_MATCHES -db $BLASTDB_DIR'/nt' -query - > $blastFileName'.out'
 

    SEQenv_print "blastn using GNU parallel took $(expr `date +%s` - $startTime) seconds to generate $blastFileName.out from $filteredOTUsFileName.fa."
else
    startTime=`date +%s`
$BLASTN_DIR/blastn -db  $BLASTDB_DIR'/nt' -query $filteredOTUsFileName'.fa' -perc_identity $MINIMUM_PERCENT_IDENT -out $blastFileName'.out' -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs staxids" -max_target_seqs $NUMBER_OF_REFERENCE_MATCHES -evalue 0.00001 -dust no -num_threads 1
    SEQenv_print "blastn took $(expr `date +%s` - $startTime) seconds to generate $blastFileName.out from $filteredOTUsFileName.fa."
fi

# Filter out low quality hits
SEQenv_print "STEP 3: Filter out low quality hits with query coverage < $MINIMUM_QUERY_COVERAGE%."
blastFilteredFileName=$blastFileName'_F'
if [ "$(skip_gen_file $blastFilteredFileName'.out')" == "true" ];then
   SEQenv_print $blastFilteredFileName'.out' already exists! Skipping this step.
else
   cat $blastFileName'.out' | awk -F"\t" -v pattern=$MINIMUM_QUERY_COVERAGE '$13>=pattern{print $0}' > $blastFilteredFileName'.out'
   SEQenv_print $blastFilteredFileName'.out' is successfully generated.
fi


# Retrieve the selected type of documents from NCBI
SEQenv_print "STEP 4: Download data from NCBI."

if [ "$(skip_gen_dir documents)" == "true" ];then
       SEQenv_print Folder documents already exists! Skipping this step.
else
    create_dirs documents

    #get the GIs list (unique, sorted)
    cut -f2 $blastFilteredFileName'.out' | cut -f2 -d\|  | sort -u > gis_unique_sorted.tsv

    #switch on text source
    if [ $TEXT_SOURCE -eq 1 ];then
        python $SEQENV_PIPELINE_DIR/scripts/envo_get_isolation_source.py -g gis_unique_sorted.tsv  > gi_isid.unsorted.tsv
        sort -un gi_isid.unsorted.tsv > gi_isid_unique_sorted.tsv
        SEQenv_print "Retrieved GenBank -isolation source- field linked to GIs. Unique entries are saved in the documents folder."

    elif [ $TEXT_SOURCE -eq 2 ];then

        #get PMID linked to the GIs
        SEQenv_print "Retrieving PMIDs linked to GIs from NCBI."
        python $SEQENV_PIPELINE_DIR/scripts/envo_get_linked_pmids.py -g gis_unique_sorted.tsv -d nucleotide  > gi_pmid.unsorted.tsv
        sort -un gi_pmid.unsorted.tsv > gi_pmid_unique_sorted.tsv

        # Download abstracts
        cut -f 2 gi_pmid_unique_sorted.tsv | sort -un > pmids_unique_sorted.tsv
        python $SEQENV_PIPELINE_DIR/scripts/envo_get_abstract.py -b pmids_unique_sorted.tsv
        SEQenv_print "Retrieved PubMed abstracts. Entries are saved in the documents folder."


    else
        echo Future case 3: use both
    fi #end-of-text-source switch

fi # end-of-generate abstracts if it does not exit

if [ $TEXT_SOURCE -eq 1 ];then
	SEQenv_print "STEP 5: Concatenate GenBank -isolation source- field IDs to blast file."
elif [ $TEXT_SOURCE -eq 2 ];then
        SEQenv_print "STEP 5: Concatenate PubMed IDs to blast file."
fi
blastPMIDFileName=$blastFilteredFileName'_PMID'

if [ "$(skip_gen_file $blastPMIDFileName'.out')" == "true" ];then
    SEQenv_print $blastPMIDFileName'.out' already exists! Skipping this step.
else
    if [ $TEXT_SOURCE -eq 1 ];then
        python $SEQENV_PIPELINE_DIR/scripts/envo_blast_concat_PMID3.py -b $blastFilteredFileName'.out' -m gi_isid_unique_sorted.tsv > $blastPMIDFileName'.out'
        rm gi_isid_unique_sorted.tsv
        rm gi_isid.unsorted.tsv
        rm gis_unique_sorted.tsv
    elif [ $TEXT_SOURCE -eq 2 ];then
        python $SEQENV_PIPELINE_DIR/scripts/envo_blast_concat_PMID3.py -b $blastFilteredFileName'.out' -m gi_pmid_unique_sorted.tsv > $blastPMIDFileName'.out'
        rm gi_pmid_unique_sorted.tsv
        rm gi_pmid.unsorted.tsv
        rm gis_unique_sorted.tsv
	rm pmids_unique_sorted.tsv
    else
        echo Future case 3: use both
    fi
    SEQenv_print $blastPMIDFileName'.out' is successfully generated.
fi


# Generate ENVO hit files
SEQenv_print "STEP 6: Run SEQenv_tagger on the documents folder and generate ENVO hits file."
envoHitsFileName=$blastFilteredFileName'_ENVO'
if [ "$(skip_gen_file $envoHitsFileName'.txt')" == "true" ];then
   SEQenv_print $envoHitsFileName'.txt' already exists! Skipping this step.
else
  cd $SEQENV_PIPELINE_DIR/SEQenv_tagger/
  ./seqenv $CURRENT_DIR/documents > $CURRENT_DIR/$envoHitsFileName'.txt'
  cd $CURRENT_DIR
  SEQenv_print $envoHitsFileName'.txt' is successfully generated.
fi

# Generate world cloud for overall community profile
SEQenv_print "STEP 7: Generate word cloud for overall community profile."
overallSampleFile=$envoHitsFileName'_overall'
if [ "$(skip_gen_file $overallSampleFile'_labels.png')" == "true" ];then
        SEQenv_print $overallSampleFile'_labels.png' already exists! Skipping this step.
else
        bash $SEQENV_PIPELINE_DIR/scripts/envo_parse_environments.sh -f $envoHitsFileName'.txt' -t $overallSampleFile'_labels' > $overallSampleFile'.csv'
        perl $SEQENV_PIPELINE_DIR/scripts/envo_ids_to_terms.pl -i $overallSampleFile'.csv' -o $SEQENV_PIPELINE_DIR/scripts/envo.obo > $overallSampleFile'_labels.csv' 2>/dev/null
        Rscript $SEQENV_PIPELINE_DIR/scripts/envo_gen_word_cloud.R  --ffile=$overallSampleFile'_labels.csv' --width=1600 --height=1600 2>/dev/null
	SEQenv_print $overallSampleFile'_labels.png' is successfully generated.
fi


# Generate frequency tables for OTUs
SEQenv_print "STEP 8: Generate frequency tables for OTUs."
OTUsFileName=$envoHitsFileName'_OTUs'
if [ "$(skip_gen_file $OTUsFileName'.csv')" == "true" ];then
   SEQenv_print $OTUsFileName'.csv' already exists! Skipping this step.
else
   #generate the frequency table
   perl $SEQENV_PIPELINE_DIR/scripts/envo_gen_OTUs_freq.pl -e $envoHitsFileName'.txt' -b $blastPMIDFileName'.out' -m $DOCUMENT_MATCH -c $DOCUMENT_COUNT > $OTUsFileName'.csv' 
   SEQenv_print $OTUsFileName'.csv' is successfully generated using $DOCUMENT_MATCH document matching and $DOCUMENT_COUNT document counting. 
   
   #normalize the frequency table (if selected)
   if [ $NORMALIZATION == "none" ];then
      SEQenv_print $OTUsFileName'.csv' is not normalized!
   else
       Rscript $SEQENV_PIPELINE_DIR/scripts/envo_process_weight_matrix.R  --mfile=$CURRENT_DIR/$OTUsFileName'.csv' --nfunction=$NORMALIZATION --ofile=normalized_matrix_temp.csv --opath=$CURRENT_DIR/
       
       mv normalized_matrix_temp.csv $OTUsFileName'.csv'
       SEQenv_print $NORMALIZATION normalization applied to $OTUsFileName'.csv' successfully!
   fi
fi


# Generate frequency tables for samples
SEQenv_print "STEP 9: Generate frequency tables for samples."
samplesFileName=$envoHitsFileName'_samples'
if [ "$(skip_gen_file $samplesFileName'.csv')" == "true" ];then
   SEQenv_print $samplesFileName'.csv' already exists! Skipping this step.
else
   Rscript $SEQENV_PIPELINE_DIR/scripts/envo_gen_samples_freq.R --spefile=$SPECIES_ABUNDANCE_FILE  --otufile=$OTUsFileName'.csv' --opath=$CURRENT_DIR/  --ofile=$samplesFileName'.csv' --pflag $PRESENCE_ABSENCE_FLAG
   SEQenv_print $samplesFileName'.csv' is successfully.
fi

# Generate dot files for frequency tables of OTUs
SEQenv_print "STEP 10: Generate dot files for frequency tables of OTUs."
if [ "$(skip_gen_dir OTUs_dot)" == "true" ];then
   SEQenv_print Folder OTUs_dot already exists! Skipping this step.
else
    create_dirs OTUs_dot
    cd OTUs_dot
    perl $SEQENV_PIPELINE_DIR/scripts/envo_extract_records.pl  -i ../$OTUsFileName'.csv' -l OTU_list.txt
    #get unique ids from the file
    uniqueIDs=$(cat OTU_list.txt)
    for id in ${uniqueIDs[@]}
    do
            #remove
            SEQenv_print "Processing $id"
            perl $SEQENV_PIPELINE_DIR/scripts/envo_gen_dot_files.pl -i "$id" -o $SEQENV_PIPELINE_DIR/scripts/envo.obo 2>/dev/null
    done
    rm OTU_list.txt
        cd ..
        SEQenv_print Folder OTUs_dot is successfully generated.
fi

# Generate dot files for frequency tables of samples
SEQenv_print "STEP 11: Generate dot files for frequency tables of samples."
if [ "$(skip_gen_dir samples_dot)" == "true" ];then
   SEQenv_print Folder samples_dot already exists! Skipping this step.
else
    create_dirs samples_dot
    cd samples_dot
        perl $SEQENV_PIPELINE_DIR/scripts/envo_gen_dot_files.pl -i $CURRENT_DIR/$samplesFileName'.csv' -o $SEQENV_PIPELINE_DIR/scripts/envo.obo 2>/dev/null
    cd ..
    SEQenv_print Folder samples_dot is successfully generated.
fi


# Generate labels for frequency tables of OTUs
OTUsLabelFileName=$OTUsFileName'_labels'
SEQenv_print "STEP 12: Generate labels for frequency tables of OTUs ($ENVO_TERM)."
if [ "$(skip_gen_file $OTUsLabelFileName'.csv')" == "true" ];then
        SEQenv_print $OTUsLabelFileName'.csv' already exists! Skipping this step.
else
        if [ "$ENVO_TERM" == "all" ];then
		perl $SEQENV_PIPELINE_DIR/scripts/envo_ids_to_terms.pl -i $OTUsFileName'.csv' -o $SEQENV_PIPELINE_DIR/scripts/envo.obo  > $OTUsLabelFileName'.csv' 2>/dev/null
        else
		perl $SEQENV_PIPELINE_DIR/scripts/envo_ids_to_terms.pl -i $OTUsFileName'.csv' -o $SEQENV_PIPELINE_DIR/scripts/envo.obo -d $ENVO_TERM > $OTUsLabelFileName'.csv' 2>/dev/null
	fi
	SEQenv_print $OTUsLabelFileName'.csv' is successfully generated.
fi

# Generate labels for frequency tables of samples
samplesLabelFileName=$samplesFileName'_labels'
SEQenv_print "STEP 13: Generate labels for frequency tables of samples ($ENVO_TERM)."
if [ "$(skip_gen_file $samplesLabelFileName'.csv')" == "true" ];then
    SEQenv_print $samplesLabelFileName'.csv' already exists! Skipping this step.
else
    if [ "$ENVO_TERM" == "all" ];then
	perl $SEQENV_PIPELINE_DIR/scripts/envo_ids_to_terms.pl -i $samplesFileName'.csv' -o $SEQENV_PIPELINE_DIR/scripts/envo.obo  > $samplesLabelFileName'.csv' 2>/dev/null
    else
	perl $SEQENV_PIPELINE_DIR/scripts/envo_ids_to_terms.pl -i $samplesFileName'.csv' -o $SEQENV_PIPELINE_DIR/scripts/envo.obo -d $ENVO_TERM > $samplesLabelFileName'.csv' 2>/dev/null
    fi
    SEQenv_print $samplesLabelFileName'.csv' is successfully generated.
fi

# Generate world clouds for samples
SEQenv_print "STEP 14: Generate word clouds for samples ($ENVO_TERM)."
if [ "$(awk 'BEGIN {FS=","} END {print NF}' $samplesLabelFileName'.csv')" -gt 4 ]; then
    if [ "$(skip_gen_dir samples_wc)" == "true" ];then
           SEQenv_print Folder samples_wc already exists! Skipping this step.
    else
            create_dirs samples_wc
            cd samples_wc
            Rscript $SEQENV_PIPELINE_DIR/scripts/envo_gen_word_cloud.R  --ffile=$CURRENT_DIR/$samplesLabelFileName'.csv' --width=2000 --height=2000
            cd ..
            SEQenv_print Folder samples_wc is successfully generated.
    fi
else
        SEQenv_print Folder samples_wc is not generated as not enough columns in $samplesLabelFileName'.csv'
fi


# Generate world clouds for OTUs ($ENVO_TERM)
SEQenv_print "STEP 15: Generate word clouds for OTUs ($ENVO_TERM)."
if [ "$(awk 'BEGIN {FS=","} END {print NF}' $OTUsLabelFileName'.csv')" -gt 4 ]; then
        if [ "$(skip_gen_dir OTUs_wc)" == "true" ];then
                SEQenv_print Folder OTUs_wc already exists! Skipping this step.
        else
                create_dirs OTUs_wc
                cd OTUs_wc
                Rscript $SEQENV_PIPELINE_DIR/scripts/envo_gen_word_cloud.R  --ffile=$CURRENT_DIR/$OTUsLabelFileName'.csv' --width=2000 --height=2000
                cd ..
                SEQenv_print Folder OTUs_wc is successfully generated.
        fi
else
        SEQenv_print Folder OTUs_wc is not generated as not enough columns in $OTUsLabelFileName'.csv'.
fi

# Generate heap maps for samples
SEQenv_print "Generate heapmap for samples ($ENVO_TERM)."
if [ "$(skip_gen_dir samples_heapmaps)" == "true" ];then
   SEQenv_print Folder samples_heapmaps already exists! Skipping this step.
else
        create_dirs samples_heapmaps
        cd samples_heapmaps
    if [ "$(awk 'BEGIN {FS=","} END {print NF}' $CURRENT_DIR/$samplesLabelFileName'.csv')" -gt 3 ]; then
            Rscript $SEQENV_PIPELINE_DIR/scripts/envo_gen_heapmap.R --ffile=$CURRENT_DIR/$samplesLabelFileName'.csv' --oname=$samplesLabelFileName --rowSize=2 --colSize=2 --width=3600 --height=6000
            SEQenv_print Generated $samplesLabelFileName'.png' successfully.
    else
        SEQenv_print $samplesLabelFileName'.png' is not generated as not enough columns in $samplesLabelFileName'.csv'.
    fi
    cd ..
        SEQenv_print Folder samples_heapmaps is successfully generated.
fi

SEQenv_print "Finished processing!"

