#!/usr/bin/python
# ***************************************************************
# Name:      envo_get_isolation_source.py
#               
# Purpose:  This scripts takes a list of unique GIds
#           retrieves their Gebank records and extracts the isolation source (IS) information contained therein.
#           each distinct (all lower case) isolation source is given a unique ID (ISID: IS_1, IS_2, ...)
# Output:   a. Every GI - isolation source is printed on standard output 
#               in the format GI \t ISID
#               multiple ISs associated with a given GI are reported in seperate lines
#               If no IS is found then the GI will not be reported
#           b. every registered IS get written in a text file named after its id
#               (e.g. IS_1.txt, IS_2.txt, ...) and placed in the "documents" folder
#               to be processed further by the text mining module
#               
# Version:   0.1 (based on envo_get_linked_pmids.py)
#
# Authors:   Umer Zeeshan Ijaz (Umer.Ijaz@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/umer.ijaz
#            Christopher Quince (Christopher.Quince@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/christopher.quince
#            Evangelos Pafilis (vagpafilis@gmail.com)
#               http://epafilis.info
# Created:   2012-11-23
# License:   Copyright (c) 2012 Computational Microbial Genomics Group, University of Glasgow, UK
#            Copyright (c) 2012 Hellenic Center for Marine Research, Greece
#
#            This program is free software: you can redistribute it and/or modify
#            it under the terms of the GNU General Public License as published by
#            the Free Software Foundation, either version 3 of the License, or
#            (at your option) any later version.
#
#            This program is distributed in the hope that it will be useful,
#            but WITHOUT ANY WARRANTY; without even the implied warranty of
#            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#            GNU General Public License for more details.
#
#            You should have received a copy of the GNU General Public License
#            along with this program.  If not, see <http://www.gnu.org/licenses/>.
# **************************************************************/     

import re,urllib,urllib2,sys,time,getopt
from xml.dom import minidom
from time import sleep
from datetime import datetime



# set http timeout to a large number
import socket
socket.setdefaulttimeout(600) # time in seconds (ie 600 -> 10 minutes)



def usage():
    print 'Usage:'
    print '\tpython envo_get_isolation_source.py -g <gid_list_input_file> > <output_file>'

def main(argv):
    gid_file=''
    try:
        opts, args = getopt.getopt(argv,"hg:",["help","gid_file="])
    except getopt.GetoptError:
        usage()
        exit(2)
    for opt, arg in opts:
        if opt == '-h':
            usage()
            exit()
        elif opt in ("-g", "--gid_file"):
            gid_file = arg

    if gid_file=="":
        usage()
        exit()
    
    process_gid_list(gid_file)



def process_gid_list(gid_file, email='pafilis@hcmr.gr', tool='Environmental Context'):
    
    #read the gids and prepare the ids to be sent to NCBI via eUtils
    INPUT = open(gid_file, "r")
    gids =INPUT.readlines()
    INPUT.close()
    
    gids_csv= ','.join (gids) 
    gids_csv = gids_csv.replace("\n","")
    
    
    #batch retrieval based on: http://www.ncbi.nlm.nih.gov/books/NBK1058/#eutils_esayers-5-4-3
    #and http://www.ncbi.nlm.nih.gov/books/NBK25498/
    
    #epost ncbi and get the web_env and query_key pointers to the results
    ####
    epost_params = {
        "db":"nucleotide",
        "tool":tool,
        "email":email,
        "id":gids_csv
    }
    epost_url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/epost.fcgi"
    
    ##TODO: place the DELAY in CONFIG file
    DELAY = 1
    sleep(DELAY)
   
    #POST request based on: http://docs.python.org/2/howto/urllib2.html
    #when "data" is used in urllib2.Request, the request is automatically an HTTP POST
    epost_req_data = urllib.urlencode(epost_params)
    epost_req = urllib2.Request(epost_url, epost_req_data)
    epost_response = urllib2.urlopen(epost_req)
    epost_response_text = epost_response.read()
    
    
    
    epost_xmldoc = minidom.parseString(epost_response_text)
    
    #NCBI pointers to the results (to be retrieved in batches)
    query_key = epost_xmldoc.getElementsByTagName('QueryKey')[0].firstChild.nodeValue
    web_env = epost_xmldoc.getElementsByTagName('WebEnv')[0].firstChild.nodeValue
    
    ##end of epost-clause
    #####################
    
    
    #efetch ncbi mulitple times and get the results in batches
    #####################
    
    #storing and assigning IDs to the isolation sources
    isolation_source_ISID_map = {}
    ISID_counter = 0
    
    #result batch size
    retmax = 250
    for retstart in range(0, len (gids), retmax):
        #print ":", retstart, retmax, len(gids), DELAY, str(datetime.now())
        efetch_params = {
            "db":"nucleotide",
            "tool":tool,
            "email":email,
            "retmode":"text",
            "rettype":"gb",
            "retstart":retstart,
            "retmax":retmax,
            "WebEnv":web_env,
            "query_key":query_key
        }
        efetch_url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi"
        
        ##TODO: place the DELAY in CONFIG file
        #DELAY = 0.3
        sleep(DELAY)
       
        #POST request based on: http://docs.python.org/2/howto/urllib2.html
        #when "data" is used in urllib2.Request, the request is automatically an HTTP POST
        efetch_req_data = urllib.urlencode(efetch_params)
        efetch_req = urllib2.Request(efetch_url, efetch_req_data)
        efetch_response = ''
	while 1:
		try:
			efetch_response = urllib2.urlopen(efetch_req)
		except IOError, e:
    			if hasattr(e, 'reason'):
        			print >> sys.stderr, 'We failed to Communicate with NCBI.'
        			print >> sys.stderr, 'Reason: ', e.reason
    			elif hasattr(e, 'code'):
        			print >> sys.stderr, 'The server couldn\'t fulfill the request.'
        			print >> sys.stderr, 'Error code: ', e.code
		else:
			break
        
	efetch_response_text = efetch_response.read()
        
        #####
        #TODO: handle time out, catch exception of some results being missed
        ####
        
        #process each banch of 'retmax' number of sequence records
        for sequence_record in efetch_response_text.split("//\n"): # split on GenBank end of record indicator

            #extract the GI from the sequence record
            gi_match = re.search(r'VERSION.*GI:(.*)', sequence_record )
            if gi_match :
                current_gid = gi_match.group(1)
                #print 'Match found: ', gi_match.group()
                #print current_gid
            #else:
                #TODO: ignore last split element, it that possible?
                #TODO: remove
                #print 'No GI match'
                
            #extract the isolation source from the sequence record (if any)
            IS_matches = re.findall(r'/isolation_source="(.+?)"\n', sequence_record, re.DOTALL)
            for IS_match in IS_matches:
                current_IS = IS_match.replace("\n","")
                current_IS = re.sub("\s{2,}"," ", current_IS)
                current_IS = current_IS.lower()
                current_ISID = ""        
                #get the ISID for the current isolation source, if new register ti    
                try: 
                    current_ISID = isolation_source_ISID_map[ current_IS ]
                except KeyError: #equivalent of Perl's if not exists
                    ISID_counter = ISID_counter+1
                    new_ISID = "IS_"+ str(ISID_counter)
                    isolation_source_ISID_map[ current_IS ] = new_ISID
                    current_ISID = isolation_source_ISID_map[ current_IS ]
                print current_gid + "\t" + current_ISID #, current_IS

    #end of efetch-clause
    #####################
    
    #generate the isolation source files
    for isolation_source in isolation_source_ISID_map:
        #print isolation_source_ISID_map[isolation_source], isolation_source
        IS_OUTPUT = open ("./documents/"+isolation_source_ISID_map[isolation_source] +".txt", "w")
        IS_OUTPUT.write( isolation_source.encode("utf-8"))
        IS_OUTPUT.close()
    
#end of process_gid_list
###########################################################################
            
if __name__ == '__main__':
    main(sys.argv[1:])
