#!/usr/bin/perl
## ***************************************************************
# Name:      envo_gen_dot_files.pl
# Purpose:   Generates the envo terms from the ids given in the header (first row)
#	     from the reference ontology envo.obo. The script then goes through each
#	     sample (record) and generates a hierarchical tree structure as a dot file
#	     comprising all the ancestoral nodes for the given node in the record. 
#	     All the observed terms are drawn as boxes and unobserved terms in the lineage 
#	     are drawn as ellipses. The boxes are then colored based on their frequency 
#	     from blue to red with blue corresponding to low frequency terms and red 
#	     corresponding to to high frequency terms, respectively. These dot files
#	     can then be viewed in Graphviz or any other visualisation tool that supports
#	     dot language.
#	     This scripts also prints the hierarchical structure of each term.  
# Version:   0.1
# Authors:   Umer Zeeshan Ijaz (Umer.Ijaz@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/umer.ijaz
#            Christopher Quince (Christopher.Quince@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/christopher.quince
#			 Evangelos Pafilis (vagpafilis@gmail.com)
#				  http://epafilis.info
# Created:   2012-10-20
# License:   Copyright (c) 2012 Computational Microbial Genomics Group, University of Glasgow, UK
#
#            This program is free software: you can redistribute it and/or modify
#            it under the terms of the GNU General Public License as published by
#            the Free Software Foundation, either version 3 of the License, or
#            (at your option) any later version.
#
#            This program is distributed in the hope that it will be useful,
#            but WITHOUT ANY WARRANTY; without even the implied warranty of
#            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#            GNU General Public License for more details.
#
#            You should have received a copy of the GNU General Public License
#            along with this program.  If not, see <http://www.gnu.org/licenses/>.
# **************************************************************/

use strict;
use OBO::Parser::OBOParser;
use Getopt::Long;

my %opts; #store the input arguments
GetOptions(\%opts,
        'input|i=s',
        'transform|t=i',
        'onto|o=s',
);

if(not defined $opts{"input"})
        {
print <<EOF;
Usage:
To generate dot files without any transformation:
        perl envo_gen_dot_files.pl -i <input_file>
To generate dot files with square root transformation:
	perl envo_gen_dot_files.pl -i <input_file> -t 2
Furthermore, by default envo.obo is picked from the current directory. If it is located,
somewhere else then use the -o switch to specify it's location.
EOF
        exit;
        }


my $csv_file = $opts{"input"};
my $ontology_file="envo.obo";
unless(not defined $opts{"onto"})
        {$ontology_file=$opts{"onto"};}
unless(-e $ontology_file)
        {
        print "Error:",$ontology_file, " not found in the current directory!\n";
        exit;
        }

my $my_parser = OBO::Parser::OBOParser->new();
my $ontology = $my_parser->work($ontology_file);

my %ENVO_hash={};
my @ENVO_terms;
my $max_value=0;
my $tmp_max;

my $data_transform=1; #1 for normal, 2 for square-root transformation
unless(not defined $opts{"transform"})
	{$data_transform=$opts{"transform"};}

open(FILE, $csv_file) or die "Can't open $csv_file\n";

# I need to optimize the following code so as not to open the csv file twice
#open the csv file, generate the tree and store the $max_value #############
my $line=<FILE>;
chomp($line);
my @tokens=split(/,/,$line);
shift(@tokens);
foreach my $tok (@tokens){
	my @my_terms=@{$ontology->get_terms($tok."\$")};
	foreach my $t (@my_terms){
		push @ENVO_terms, $t->id();
		####print $t->id,"(",$t->name(),")";
		my $test=get_levels($t->id(),0);
	}	
	####print "\n";
}
while($line=<FILE>){
  chomp($line);
  @tokens = split(/,/,$line);
  my $filename=shift(@tokens);
  
  if($data_transform==1){
  }
  elsif($data_transform==2){
  	for (my $i=0;$i<scalar(@tokens);$i++){
		$tokens[$i]=sqrt($tokens[$i]);
  	}
  }
  $tmp_max = (sort { $b <=> $a } @tokens)[0];
  if ($tmp_max > $max_value)
	{
	$max_value=$tmp_max;
	}
}
close(FILE);
###########################################################################


# print recovered tree ####################################################
#my($key, $value);
#while ( ($key, $value) = each(%ENVO_hash) ) {
#    print "$key:\t";
#       foreach (@{$value}){
#               print "$_\t";
#       }
#    print "\n";
#}
##########################################################################

# generate dot files #####################################################
my $left_id;
my $right_id;
my @col;
my @hsv;
open(IN, $csv_file) or die;
$line=<IN>;
while($line=<IN>){
  chomp($line);
  @tokens = split(/,/,$line);
  my $filename=shift(@tokens);
  open(OUT,">".$filename.".dot");
  print OUT "digraph ENVO_Ontology {\n";
  for (my $i=0; $i<scalar(@ENVO_terms); $i++)
        {
	if($tokens[$i]!=0){
		if($data_transform==1){
  			@col=get_color(scale_value($tokens[$i],0,$max_value,-1.0,1.0),-1.0,1.0);
			}
  		elsif($data_transform==2){
			@col=get_color(scale_value(sqrt($tokens[$i]),0,$max_value,-1.0,1.0),-1.0,1.0);
  			}
		@hsv=rgb_to_hsv($col[0],$col[1],$col[2]);
		$left_id=node_label($ENVO_terms[$i],2);
		print OUT $left_id," [shape=box,fillcolor=\"$hsv[0] $hsv[1] $hsv[2]\",style=filled];\n";
		}
        }
  while ( my($key, $value) = each(%ENVO_hash) ) {
       $left_id=node_label($key,2);
       foreach (@{$value}){
               $right_id=node_label($_,2);
               print OUT "edge [label=\"\"];\n";
               print OUT $left_id," -> ",$right_id,";\n";
       }
  }
  print OUT "}";
  close(OUT);
 

}
close(IN);

##########################################################################
# Recursively generate the parents of the given node up to the root node
# and save the tree structure in %ENVO_hash. Furthermore, return the depth
# of the tree as levels. If there are multiple paths to the root node then
# only the longest is returned as $out

sub get_levels{
	my ($id,$level) = @_;
	my $out=$level;
	my $tmp=$out;
	my $interesting_term=$ontology->get_term_by_id($id);
	my @children=@{$ontology->get_parent_terms($interesting_term)};
	foreach my $c (@children){
		####print "\n";
		for(my $i=0;$i<=$level;$i++){
			####print "\t";
		}
		
		#populate ENVO_hash ######################################
		if (not defined @{$ENVO_hash{$id}})
			{
			push @{$ENVO_hash{$id}}, $c->id();
			}
		else	{
			unless($c->id() ~~ @{$ENVO_hash{$id}}) #enter unique ENVO ID
				{
				push @{$ENVO_hash{$id}}, $c->id();
				}
			}

		########################################################		

		####print $c->id,"(",$c->name(),")";
		$tmp=get_levels($c->id(),$level+1);
		if ($tmp > $out){
			$out = $tmp;
			}
	}
	return $out;
}

# Return a RGB colour value given a scalar v in the range [vmin,vmax]
# In this case each colour component ranges from 0 (no contribution) to
# 1 (fully saturated), modifications for other ranges is trivial.
# The colour is clipped at the end of the scales if v is outside
# the range [vmin,vmax]
# Ref:http://stackoverflow.com/questions/7706339/grayscale-to-red-green-blue-matlab-jet-color-scale

sub get_color{
   my ($v,$vmin,$vmax) = @_;
   my @c=(1.0,1.0,1.0); #white
   my $dv;

   if ($v < $vmin){
    $v = $vmin;
    }
   if ($v > $vmax){
      $v = $vmax;
   }
   $dv = $vmax - $vmin;

   if ($v < ($vmin + 0.25 * $dv)) {
      $c[0] = 0;
      $c[1] = 4 * ($v - $vmin) / $dv;
   } 
   elsif ($v < ($vmin + 0.5 * $dv)) {
      $c[0] = 0;
      $c[2] = 1 + 4 * ($vmin + 0.25 * $dv - $v) / $dv;
   } 
   elsif ($v < ($vmin + 0.75 * $dv)) {
      $c[0] = 4 * ($v - $vmin - 0.5 * $dv) / $dv;
      $c[2] = 0;
   } 
   else {
      $c[1] = 1 + 4 * ($vmin + 0.75 * $dv - $v) / $dv;
      $c[2] = 0;
   }

   return @c;
}
# scale the values between $newMin and $newMax
sub scale_value{
my ($in,$oldMin,$oldMax,$newMin,$newMax) = @_;
return ($in/(($oldMax-$oldMin)/($newMax-$newMin)))+$newMin;
}

# dot file accepts a floating hsv value for color
sub rgb_to_hsv{
   my ($rc, $gc, $bc) = @_;
   my @c=(1.0,1.0,1.0);
   my ($min,$max,$delta);

   
   if($gc > $bc){
	$max=$gc;
	}
   else {
	$max=$bc;
	}

   if($rc > $max){
	$max=$rc;
	}

   if($gc < $bc){
        $min=$gc;
        }
   else {
        $min=$bc;
        }

   if($rc < $min){
        $min=$rc;
        }

   $delta = $max - $min;
   $c[2] = $max;

   if ($max != 0.0)
      {
      $c[1] = $delta / $max;
      }
   else {
      $c[1] = 0.0;
      }

   if ($c[1] == 0.0) {
      $c[0] = 0.0; 
   }
   else {
      if ($rc == $max){
	 $c[0] = ($gc - $bc) / $delta;
	}
      elsif ($gc == $max) {
	 $c[0] = 2 + ($bc - $rc) / $delta;
	}
      elsif ($bc == $max){
	 $c[0] = 4 + ($rc - $gc) / $delta;
       }
 	
      $c[0] = $c[0]*60.0;
      if ($c[0] < 0){
	 $c[0] = $c[0] + 360.0;
      }
      $c[0]=$c[0]/360.0;
    }
    return @c;
}

sub node_label{

my ($id,$which) = @_;
my ($ret_name,$ret_term);
$ret_name="undef";
if($id!~/ENVO/)
	{
	return $ret_name;
	}
if ($which == 1)
	{
		$ret_name=$id;
		$ret_name=~tr/:/_/;
	}
elsif ($which == 2){
		$ret_term=$ontology->get_term_by_id($id);
		$ret_name=$ret_term->name();
		$ret_name=~tr/ /_/;
		}
	
return $ret_name;
}
