#!/usr/bin/perl
# ***************************************************************
# Name:      envo_extract_ids_level.pl
# Purpose:   This script uses a $cutoff_level to collapse the ENVO tree of observed 
#	     ENVO term to the given hierarchical level. For each observed ENVO term
#	     there are multiple paths to the root node. As an example, consider 
#	     "glacial_lake". From envo.obo, we recover the following four paths:
#	     Path 1:
#		ENVO:00002297(environmental_feature),ENVO:00000000(geographic_feature),ENVO:00000012(hydrographic_feature),
#		ENVO:00000131(glacial_feature),ENVO:00000488(glacial_lake)
#	     Path 2:
#		ENVO:00010483(environmental_material),ENVO:00002006(water),ENVO:00000012(hydrographic_feature),
#		ENVO:00000131(glacial_feature),ENVO:00000488(glacial_lake)
#	     Path 3:
#		ENVO:00002297(environmental_feature),ENVO:00000000(geographic_feature),ENVO:00000012(hydrographic_feature),
#		ENVO:00000063(water_body),ENVO:00000020(lake),ENVO:00000488(glacial_lake)
#	     Path 4:
#		ENVO:00010483(environmental_material),ENVO:00002006(water),ENVO:00000012(hydrographic_feature),
#		ENVO:00000063(water_body),ENVO:00000020(lake),ENVO:00000488(glacial_lake)
#
#	    At cutoff of 1, we have two unique nodes: environmental_feature, environmental material
#	    At cutoff of 2, we have two unique nodes: water and geographic_feature
#	    At cutoff of 3, we have one unique node: hydrographic_feature
#
#	    If we collapse the tree for a given observed ENVO term and there are multiple paths with multiple terms at each
#	    level then each term will get the same frequency count as the observed ENVO term.
#	    For example, given two samples of glacial_lake, i.e.
#
#			glacial_lake
#		T_1     34
#		T_2     12
#
#	    after collapsing at level 2, they will become,
#
#			water	geographic_feature
#		T_1	34	34
#		T_2	12	12
#
#
#	    Now, if we collapse another term, say "shale" into "rock" and geographic feature" at cutoff of 2 with original 
#	    frequency count as
#
#			shale
#		T_1	10
#		T_2  	23
#
#	   then final picture would be (frequencies of the common terms will be added together, geographic feature in this case)
#
#			water	geographic feature	rock
#		T_1	34	44			10
#		T_2	12	35			23
#
#
# Version:   0.1
# Authors:   Umer Zeeshan Ijaz (Umer.Ijaz@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/umer.ijaz
#            Christopher Quince (Christopher.Quince@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/christopher.quince
# Created:   2012-11-04
# License:   Copyright (c) 2012 Computational Microbial Genomics Group, University of Glasgow, UK
#
#            This program is free software: you can redistribute it and/or modify
#            it under the terms of the GNU General Public License as published by
#            the Free Software Foundation, either version 3 of the License, or
#            (at your option) any later version.
#
#            This program is distributed in the hope that it will be useful,
#            but WITHOUT ANY WARRANTY; without even the implied warranty of
#            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#            GNU General Public License for more details.
#
#            You should have received a copy of the GNU General Public License
#            along with this program.  If not, see <http://www.gnu.org/licenses/>.
# **************************************************************/

use strict;
use OBO::Parser::OBOParser;
use Getopt::Long;

my %opts; #store the input arguments
GetOptions(\%opts,
        'input|i=s',
        'cutoff_level|c=i',
        'onto|o=s',
);

if(not defined $opts{"input"})
        {
print <<EOF;
Usage:
To generate a csv file:
        perl envo_extract_ids_level.pl -i <input_file> -c <cutoff_level> > <output_file>
Furthermore, by default envo.obo is picked from the current directory. If it is located,
somewhere else then use the -o switch to specify it's location.
EOF
        exit;
        }


my $csv_file = $opts{"input"};
my $ontology_file="envo.obo";
unless(not defined $opts{"onto"})
        {$ontology_file=$opts{"onto"};}
unless(-e $ontology_file)
        {
        print "Error:",$ontology_file, " not found in the current directory!\n";
        exit;
        }

my $cutoff_level=10;
unless(not defined $opts{"cutoff_level"})
        {$cutoff_level=$opts{"cutoff_level"};}

my $my_parser = OBO::Parser::OBOParser->new();
my $ontology = $my_parser->work($ontology_file);

my %ENVO_hash={};
my @ENVO_terms;


open(FILE, $csv_file) or die "Can't open $csv_file\n";

my $tree_ind;

#open the csv file, generate the tree and populate %ENVO_hash #############
my $line=<FILE>;
chomp($line);
my @tokens=split(/,/,$line);
shift(@tokens);
foreach my $tok (@tokens){
	my @my_terms=@{$ontology->get_terms($tok."\$")};
	foreach my $t (@my_terms){
		push @ENVO_terms, $t->id();
		$tree_ind=1;
		my $test=get_levels2($t->id(),2,$t->id()."(".node_label($t->id(),2).")");
	}	
}
###########################################################################

# print recovered paths ###################################################
#my($key, $value);
#while ( ($key, $value) = each(%ENVO_hash) ) {
#    print "$key:\n";
#       foreach (@{$value}){
#               print "\t$_\n";
#       }
#    print "\n";
#}
###########################################################################

#generate new ENVO terms based on the cutoff_level#########################
my @ENVO_terms_new=();
my %ENVO_id_hash={};
foreach my $ENVO_term (@ENVO_terms)
	{
	my @recovered_paths=@{$ENVO_hash{$ENVO_term}};
	my @recovered_ids=();
	
	#pass 1 to get the maximum path lengths
	my $max_path_length=0;
	foreach my $recovered_path (@recovered_paths){
		my @path_array=split(/,/,$recovered_path);
		if(scalar(@path_array)>$max_path_length){$max_path_length=scalar(@path_array)};
		}	
	
	#pass 2 to recover the ids
	my $actual_cutoff_level;
	
	if($cutoff_level>$max_path_length){$actual_cutoff_level=$max_path_length;}
	else {$actual_cutoff_level=$cutoff_level;}
	foreach my $recovered_path (@recovered_paths){
		my @path_array=split(/,/,$recovered_path);
		if(scalar(@path_array)>=$actual_cutoff_level)
			{
			my $recovered_id=$path_array[$actual_cutoff_level-1];
			$recovered_id=~/(.*)\(/;
			push(@recovered_ids,$1);
			}
		}
	@recovered_ids=uniq(@recovered_ids);

	#populate %ENVO_id_hash with ids at the $actual_cutoff_level
	foreach my $recovered_id (@recovered_ids){
		push(@{$ENVO_id_hash{$ENVO_term}},$recovered_id);
		}

        #now populate @ENVO_terms_new with unique ids
	foreach my $recovered_id(@recovered_ids){
		unless($recovered_id ~~ @ENVO_terms_new)
			{
			push(@ENVO_terms_new,$recovered_id);
			}
		}
	}
##########################################################################

# print recovered ids ####################################################
#my($key, $value);
#while ( ($key, $value) = each(%ENVO_id_hash) ) {
#    print "$key:\n";
#       foreach (@{$value}){
#               print "\t$_\n";
#       }
#    print "\n";
#}
##########################################################################

print "Samples";
foreach my $ENVO_term (@ENVO_terms_new){
	$ENVO_term=~/ENVO:(.*)/;
	print ",",$1;
}
print "\n";
while($line=<FILE>){
  chomp($line);
  @tokens = split(/,/,$line);
  my $sample_name=shift(@tokens);
  my $f_ind=0;
  my @record=();
  for(my $i=0;$i<scalar(@ENVO_terms_new);$i++){$record[$i]=0;}
  foreach my $tok(@tokens){
	my @recovered_ids=@{$ENVO_id_hash{$ENVO_terms[$f_ind]}};
	foreach my $recovered_id (@recovered_ids){
		my( $index )= grep { $ENVO_terms_new[$_] eq $recovered_id } 0..$#ENVO_terms_new;
		if(defined $index){
			$record[$index]=$record[$index]+$tok;
			}
	}  	
	$f_ind=$f_ind+1;
	}
print $sample_name,",",join(",",@record),"\n";	  
}
close(FILE);
###########################################################################


###########################################################################
# Recursively generate the parents of the given node up to the root node
# and save all the paths in %ENVO_hash. Furthermore, return the depth
# of the tree as levels. 

sub get_levels2{
        my ($id,$level,$path) = @_;
        my $out=$level;
        my $tmp=$out;
        my $interesting_term=$ontology->get_term_by_id($id);
        my @children=@{$ontology->get_parent_terms($interesting_term)};
        my $local_ind=0;
        foreach my $c (@children){
                if ($local_ind>0){$tree_ind=$tree_ind+1;}
                $tmp=get_levels2($c->id(),$level+1,$path.",".$c->id()."(".node_label($c->id(),2).")");
                if ($tmp > $out){
                        $out = $tmp;
                        }
        $local_ind=$local_ind+1;
        }
	if($local_ind==0){
			 #reverse the $path to obtain path from root node to the leaf node
			 my $root_to_leaf_path=join(',',reverse(split(/,/,$path)));
			 #following three lines extract the envo id from the path
			 my @path_array=split(/,/,$path);
			 my $id_string=shift(@path_array);
			 $id_string=~/(.*)\(/; 
                	 #populate ENVO_hash with $root_to_leaf_path
                	 push @{$ENVO_hash{$1}}, $root_to_leaf_path;
			 #print "\n", $1;
			 #print "\n",join(',',reverse(split(/,/,$path)));
			 }
        return $out;
}


sub node_label{

my ($id,$which) = @_;
my ($ret_name,$ret_term);
$ret_name="undef";
if($id!~/ENVO/)
	{
	return $ret_name;
	}
if ($which == 1)
	{
		$ret_name=$id;
		$ret_name=~tr/:/_/;
	}
elsif ($which == 2){
		$ret_term=$ontology->get_term_by_id($id);
		$ret_name=$ret_term->name();
		$ret_name=~tr/ /_/;
		}
	
return $ret_name;
}

sub uniq {
    my %seen = ();
    my @r = ();
    foreach my $a (@_) {
        unless ($seen{$a}) {
            push @r, $a;
            $seen{$a} = 1;
        }
    }
    return @r;
}
