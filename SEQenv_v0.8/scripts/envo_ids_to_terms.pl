#!/usr/bin/perl
# ***************************************************************
# Name:      envo_ids_to_terms.pl
# Purpose:   Generates the envo terms from the ids given in the header (first row)
#	     from the reference ontology file provided as envo.obo.
#	     Since envo.obo is hierarchical and has three root nodes, this script
#	     can also recover a subset of ids corresponding to the root node of interest 
# Version:   0.1
# Authors:   Umer Zeeshan Ijaz (Umer.Ijaz@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/umer.ijaz
#            Christopher Quince (Christopher.Quince@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/christopher.quince
# Created:   2012-10-20
# License:   Copyright (c) 2012 Computational Microbial Genomics Group, University of Glasgow, UK
# 
#            This program is free software: you can redistribute it and/or modify
#            it under the terms of the GNU General Public License as published by
#            the Free Software Foundation, either version 3 of the License, or
#            (at your option) any later version.
#
#            This program is distributed in the hope that it will be useful,
#            but WITHOUT ANY WARRANTY; without even the implied warranty of
#            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#            GNU General Public License for more details.
#
#            You should have received a copy of the GNU General Public License
#            along with this program.  If not, see <http://www.gnu.org/licenses/>.
# **************************************************************/

use strict;
use OBO::Parser::OBOParser;
use Getopt::Long;

my %opts; #store the input arguments
GetOptions(\%opts,
	'input|i=s',
	'id|d=s',
	'onto|o=s',
);

if(not defined $opts{"input"})
	{
print <<EOF;
Usage:
To convert whole file, use:
	perl envo_ids_to_terms.pl -i <input_file> > <output_file>	
To recover records for those envo ids that have "environmental material" as
a root node, use:
	perl envo_ids_to_terms.pl -i <input_file> -d ENVO:00010483 > <output_file>
To recover records for those envo ids that have "environmental feature" as
a root node, use: 
	perl envo_ids_to_terms.pl -i <csv_file> -d ENVO:00002297 > <output_file>
You can also specify any other envo id.	
Furthermore, by default envo.obo is picked from the current directory. If it is located,
somewhere else then use the -o switch to specify it's location.
EOF
	exit;
	}

my $csv_file = $opts{"input"};
my $ontology_file="envo.obo";
unless(not defined $opts{"onto"})
	{$ontology_file=$opts{"onto"};}
unless(-e $ontology_file)
	{
	print "Error:",$ontology_file, " not found in the current directory!\n";
	exit;
	}
my $my_parser = OBO::Parser::OBOParser->new();
my $ontology = $my_parser->work($ontology_file);
my $root_node = -1;
unless(not defined $opts{"id"})
	{$root_node=$opts{"id"};}

my @ENVO_terms;
my @ENVO_terms_flag;
my @ids;
my @values;
my ($i,$j);

open(FILE, $csv_file) or die "Can't open $csv_file\n";

my $line=<FILE>;
chomp($line);
my @tokens=split(/,/,$line); #get header information
shift(@tokens); #remove the first keyword

my $cols=scalar(@tokens);
foreach my $tok (@tokens){
	my @my_terms=@{$ontology->get_terms($tok."\$")};
	foreach my $t (@my_terms){
		push @ENVO_terms, node_label($t->id(),2);
		if($root_node==-1){
			push @ENVO_terms_flag,1;
			}
		else{
			push @ENVO_terms_flag, should_we_include_term($t->id(),$root_node);
		    }	
	}	
}

# retrieve all records and store them in @values
# store sample names in @ids
while($line = <FILE>){
  chomp($line);
  @tokens = split(/,/,$line);
  push(@ids, shift(@tokens));
  my $j = 0;
  foreach my $tok(@tokens){
    $values[$i][$j] = $tok;
    $j++;
  }
  $i++;
}
close(FILE);
my $rows=$i;

print "Samples";
for($i=0;$i<$cols;$i++)
	{
	if($ENVO_terms_flag[$i]){
		print ",",$ENVO_terms[$i];
		}
	}
for($i=0;$i<$rows;$i++)
        {
        print "\n",$ids[$i];
        for($j=0;$j<$cols;$j++)
                {
		if($ENVO_terms_flag[$j]){
                	print ",",$values[$i][$j];
			}
                        
                }
        }

# retrieve node label for the given $id
# and replace spaces with underscores
sub node_label{

my ($id,$which) = @_;
my ($ret_name,$ret_term);
$ret_name="undef";
if($id!~/ENVO/)
        {
        return $ret_name;
        }
if ($which == 1)
        {
                $ret_name=$id;
                $ret_name=~tr/:/_/;
        }
elsif ($which == 2){
                $ret_term=$ontology->get_term_by_id($id);
                $ret_name=$ret_term->name();
                $ret_name=~tr/ /_/;
                }

return $ret_name;
}

# loop through all the ancestor nodes and if match is
# is found against $ref id then return 1 otherwise 0

sub should_we_include_term{
my ($id,$ref) = @_;
my $interesting_term=$ontology->get_term_by_id($id);
my @ancestors = @{$ontology->get_ancestor_terms($interesting_term)};
my @ids_list; #store ids of ancestor nodes		
foreach my $t (@ancestors) {
	push @ids_list, $t->id();
}
if ($ref ~~ @ids_list)
	{
	return 1;
	}
else{
	return 0;
    }

}
