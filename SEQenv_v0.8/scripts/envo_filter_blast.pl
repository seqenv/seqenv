#!/usr/bin/perl
# ***************************************************************
# Name:      envo_filter_blast.pl
# Purpose:   This scripts takes a blast output file and corresponding
#	     fasta file and filters out records with a minimum cutoff
#	     of quality.
#	     This script can filter blast files generated through the
#	     the following command:
#
#	     blastn -db <path_to_nt> -query <fasta_file> 
#	     -out <blast_file> -outfmt “6” -num_threads 8 
#	     -max_target_seqs 100	
#	
# Version:   0.1
# Authors:   Umer Zeeshan Ijaz (Umer.Ijaz@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/umer.ijaz
#            Christopher Quince (Christopher.Quince@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/christopher.quince
# Created:   2012-10-20
# License:   Copyright (c) 2012 Computational Microbial Genomics Group, University of Glasgow, UK
#
#            This program is free software: you can redistribute it and/or modify
#            it under the terms of the GNU General Public License as published by
#            the Free Software Foundation, either version 3 of the License, or
#            (at your option) any later version.
#
#            This program is distributed in the hope that it will be useful,
#            but WITHOUT ANY WARRANTY; without even the implied warranty of
#            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#            GNU General Public License for more details.
#
#            You should have received a copy of the GNU General Public License
#            along with this program.  If not, see <http://www.gnu.org/licenses/>.
# **************************************************************/

use strict;
use Getopt::Long;

my %opts; #store the input arguments
GetOptions(\%opts,
        'fasta_file|f=s',
        'blast_file|b=s',
	'cutoff|c=s',
);

if((not defined $opts{"fasta_file"}) || (not defined $opts{"blast_file"})) 
        {
print <<EOF;
Usage:
        perl envo_filter_blast.pl -f <fasta_file> -b <blast_file> -c <cutoff> > <output_file>
EOF
        exit;
        }

my $minimum_cutoff = 0.97;
unless(not defined $opts{"cutoff"})
	{$minimum_cutoff=$opts{"cutoff"};}

my $fasta_file = $opts{"fasta_file"};
my $blast_file = $opts{"blast_file"};

my @lengths = ();
my %mapID = {};
my $count = 0;
open(FILE,$fasta_file) or die "Can't open $fasta_file\n";

while(my $line = <FILE>){
    chomp($line);

    if($line =~ />(.*)/){
	my $id = $1;
	$mapID{$id} = $count;
	$count++;
	$line = <FILE>;
	chomp($line);
	my $length = length($line);
    	push(@lengths,$length);
    }
}

close(FILE);

my %PMMap_hash = {};

open(FILE, $blast_file) or die "Can't open $blast_file\n";

while(my $line = <FILE>){
  chomp($line);
  my @tokens = split(/\t/,$line);

  my $id = $tokens[0];

  my $hit = $tokens[1];

  my $phit = $tokens[2];
  
  my $hlength = $tokens[7] - $tokens[6] + 1;
  my $ilength = $lengths[$mapID{$id}];
  my $flength = $hlength/$ilength;
  
  my $cov = ($flength*$phit)/100;
  my $thit = pop(@tokens);
  
  if($cov >= $minimum_cutoff){
    print "$line\n";
  }
}

close(FILE);

