#!/bin/bash
# ***************************************************************
# Name:      envo_parse_environments.sh
# Purpose:   This script parses the output produced by the environments program and generates a
#	     comma separated list of terms sorted by frequency count
#
# Version:   0.1
# Authors:   Umer Zeeshan Ijaz (Umer.Ijaz@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/umer.ijaz
# Created:   2013-02-22
# License:   Copyright (c) 2012 Computational Microbial Genomics Group, University of Glasgow, UK
#
#            This program is free software: you can redistribute it and/or modify
#            it under the terms of the GNU General Public License as published by
#            the Free Software Foundation, either version 3 of the License, or
#            (at your option) any later version.
#
#            This program is distributed in the hope that it will be useful,
#            but WITHOUT ANY WARRANTY; without even the implied warranty of
#            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#            GNU General Public License for more details.
#
#            You should have received a copy of the GNU General Public License
#            along with this program.  If not, see <http://www.gnu.org/licenses/>.
# **************************************************************/ 
ENVO_FIELD_TITLE='freq'
inputFile=''
while getopts ":f:t:" opt; do
  case $opt in
    f)
      inputFile=$OPTARG
      ;;
    t)
      ENVO_FIELD_TITLE=$OPTARG
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      echo "Usage: envo_parse_environments.sh -f <environment_file> -n <field_label>  > <output_file>"
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
     echo "Usage: envo_parse_environments.sh -f <environment_file> -n <field_label>  > <output_file>" 
     exit 1
      ;;
  esac
done
export ENVO_FIELD_TITLE
cut -f 5 $inputFile | sort | uniq -c | sort -nrk1 | awk 'BEGIN{print "terms,"ENVIRON["ENVO_FIELD_TITLE"]}{print $2","$1}' | awk -F "," '{
     if (max_nf < NF)
          max_nf = NF
     max_nr = NR
     for (x = 1; x <= NF; x++)
          vector[x, NR] = $x
}

END {
     for (x = 1; x <= max_nf; x++) {
          for (y = 1 ; y<= max_nr; y++)
               if(y == max_nr)
		  printf("%s", vector[x, y])
	       else
		  printf("%s,", vector[x, y])
          printf("\n")
     }
}'
unset ENVO_FIELD_TITLE 
