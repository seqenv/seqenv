#!/usr/bin/perl
# ***************************************************************
# Name:      envo_gen_OTUs_freq.pl
# Purpose:   This scripts takes a blast output file with pubmed ids
#         (generated through envo_blast_concat_PMID.py) and a 
#         text file with envo terms and generates a file
#         with rows as OTUs and columns as frequency of envo terms.
#         At the moment, frequencies of multiple hits to pubmed ids
#         are aggregated together.     
# Version:   0.2
# Authors:   Umer Zeeshan Ijaz (Umer.Ijaz@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/umer.ijaz
#            Christopher Quince (Christopher.Quince@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/christopher.quince
#            Evangelos Pafilis (vagpafilis@gmail.com)
#               http://epafilis.info
# Last modified:2013-07-20
# License:   Copyright (c) 2012 Computational Microbial Genomics Group, University of Glasgow, UK
#            Copyright (c) 2012 Hellenic Center for Marine Research, Greece
#
#            This program is free software: you can redistribute it and/or modify
#            it under the terms of the GNU General Public License as published by
#            the Free Software Foundation, either version 3 of the License, or
#            (at your option) any later version.
#
#            This program is distributed in the hope that it will be useful,
#            but WITHOUT ANY WARRANTY; without even the implied warranty of
#            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#            GNU General Public License for more details.
#
#            You should have received a copy of the GNU General Public License
#            along with this program.  If not, see <http://www.gnu.org/licenses/>.
# **************************************************************/
use strict;
use Getopt::Long;
use Data::Dumper;

my %opts; #store the input arguments
GetOptions(\%opts,
        'envo_file|e=s',
        'blast_file|b=s',
        'doc_match|m=s',
        'doc_count|c=s',
);

if((not defined $opts{"envo_file"}) || (not defined $opts{"blast_file"}))
{
print <<EOF;
Usage:

perl envo_gen_OTUs_freq.pl -e <envo_file> -b <blast_file> [-m <mulitple|single>] [-c <aggr|freq>] > <output_file>
    
Required Parameters
-e <envo_file>
-b <blast_file>

Optional Paramters
-m: document match: multiple [default] / single
    an OTU may my mapped to multiple GIs and via them to relevant literature
    
    "multiple" counts how many times a single piece of literature gets mapped to an OTU
    and considers that in subsquent multiplications (e.g in calculating the occurrences of a term in
    the scientific documents linked with an OTU)
    
    "single" considers that single piece of literature only once (similar to a "presence/absence" analysis)
    
-c: document count: aggr (agrregative) / freq (document frequency weight applies) [default]
    "aggr" simply adds the occurrences of a term in the scientific documents linked with an OTU
    
    "freq" weights the aggregative score by the percent of the OTU-linked documents in which the terms occur
    in relation to the total number of the OTU-linked documents
EOF
exit;
}


# sets 'doc_match', and 'doc_count' to their defaults no invalid value had been speficied
# or if an invalid value has been speficied
if( not defined $opts{"doc_match"} ){
    $opts{"doc_match"} = "multiple";
}else {
    if( $opts{"doc_match"} ne "multiple" and $opts{"doc_match"} ne "single"){
        $opts{"doc_match"} = "multiple";
    }
}
if( not defined $opts{"doc_count"}){
    $opts{"doc_count"} = "freq";
}
else {
    if( $opts{"doc_count"} ne "aggr" and $opts{"doc_count"} ne "freq" ){
        $opts{"doc_count"} = "freq";
    }
}


my $envo_file = $opts{"envo_file"};
my $blast_file = $opts{"blast_file"};
my $doc_match = $opts{"doc_match"};
my $doc_count = $opts{"doc_count"};


open(FILE, $envo_file) or die;

my $nID = 0;
my $nTags = 0;

my @pmid = ();
my @tags = ();
my @freq = ();

my %PMID_hash = {};
my %Tags_hash = {};
my %OTUs_hash = {};

#hash of hashes to store the number of times a PMID has been mapped to an OTU
my %OTU_PMID_count = ();

#read ENVIRONMENTS output
while(my $line = <FILE>){
    chomp($line);

    my @tokens = split(/\t/,$line);

    my $nTokens = scalar(@tokens);
    if($nTokens < 5){
        my $aline = <FILE>;
        chomp($aline);
        $line = $line.$aline;
        @tokens = split(/\t/,$line);
    }
    #print "$nTokens\n";

    $tokens[0] =~/^(.+)\.txt/; #ISID contain also text, unlike the PMIDs
    my $pmid = $1;
    my $env = pop(@tokens);
    if($PMID_hash{$pmid} eq undef){
        $PMID_hash{$pmid} = $nID;
        push(@pmid,$pmid);
        $nID++;
    }

    if($Tags_hash{$env} eq undef){
        $Tags_hash{$env} = $nTags;
        push(@tags,$env);
        $nTags++;
    }
    
    $freq[$PMID_hash{$pmid}][$Tags_hash{$env}]++;
}

close(FILE);

#BUGFIX(UZI,20/07/2013): Entries in $Freq with undefined values are converted to zero
for(my $i = 0; $i < $nID; $i++){
    for(my $j = 0; $j < $nTags; $j++){
        if($freq[$i][$j] eq undef){
                $freq[$i][$j]=0;
        }
    }
}


# For debugging purposes ############################################
#print "$nID $nTags\n";
#for(my $i = 0; $i < $nID; $i++){
#    for(my $j = 0; $j < $nTags; $j++){
#        if($freq[$i][$j] > 0){
#            print "i=$i pmid=$pmid[$i] j=$j tags[j]=$tags[$j] freq[i][j]=$freq[$i][$j]\n";
#       }
#    }
#}
#####################################################################

########### Extract OTUs to Pubmed ID mapping #######################
# We will store the mapping in the OTUs_hash. By default multiple values
# for a single keys are not allowed in perl hashes, so a workaround is
# to map a key to an array reference and then populate the array.
#
# Another workaround is a hash of hashes
# e.g. $OTU_PMID_count {$OTU_name }{ $pmid } = $noCounts

my $PUBMED_COL=14;
open(FILE, $blast_file) or die;
my @blasted_OTUs;
while(my $line = <FILE>){
    chomp($line);
    my @tokens = split(/\t/,$line);
    if($tokens[$PUBMED_COL]!="-1")
    {
        
        #store the number of times a PMID has been mapped to an OTU
        my $OTU_name = $tokens[0];
        my $pmid = $tokens[$PUBMED_COL];
        if (not defined $OTU_PMID_count {$OTU_name }{ $pmid })
        {#initialize
            $OTU_PMID_count {$OTU_name }{ $pmid } = 1
        }
        else
        {
            $OTU_PMID_count {$OTU_name }{ $pmid }++;
        }

        #store OTU to list of PMIDs mapping
        #TODO: replace in usages with the above (low priority)
        if (not defined @{$OTUs_hash{$tokens[0]}})
            {
            push @{$OTUs_hash{$tokens[0]}}, $tokens[$PUBMED_COL];
            push @blasted_OTUs, $tokens[0];
        }
        else    {
            unless ($tokens[$PUBMED_COL] ~~ @{$OTUs_hash{$tokens[0]}}) #enter unique pubmed ids
                {
                push @{$OTUs_hash{$tokens[0]}}, $tokens[$PUBMED_COL];
                }
        }
    }
}
close(FILE);






my $nOTUs=scalar(@blasted_OTUs);

#For debugging purposes #############################################
#my($key, $value); 
#while ( ($key, $value) = each(%OTUs_hash) ) {
#   print "$key:\t";
#    foreach (@{$value}){
#        print "$_\t";
#    }
#    print "\n";
#}
#####################################################################



######## case: $doc_match eq "single" and $doc_count eq "aggr"
######################################################################
if ( $doc_match eq "single" and $doc_count eq "aggr")
{
    ########### Generate S X E aggregated frequency matrix ##############
    my @aggr_Freq=();
    
    for(my $i = 0; $i < $nOTUs; $i++){
        for(my $j = 0; $j < $nTags; $j++){
            $aggr_Freq[$i][$j]=0;
            foreach (@{$OTUs_hash{$blasted_OTUs[$i]}}) #OTUs to Pubmed ID
            {
		#BUGFIX(UZI,20/07/2013): %PMID_hash doesn't contain those entries which are not found in envo text file, i.e., no environmental terms found, so unless clause is inserted
		unless ($PMID_hash{$_} eq undef) {
                	$aggr_Freq[$i][$j]=$aggr_Freq[$i][$j]+$freq[$PMID_hash{$_}][$j];
		}
            }
        }
    }
    # For debugging purposes ############################################	
    #for(my $i=0; $i < $nOTUs; $i++)
    #    {
    #    print "\n\n\n";
    #    for(my $j=0; $j < $nTags; $j++)
    #        {
    #        print "$aggr_Freq[$i][$j]  ";
    #        }
    #    }
    #####################################################################
        
    my ($i,$j);
    print "OTUs,";
    for($j = 0; $j < $nTags -1 ;$j++){
        print "$tags[$j],"
    }
    print "$tags[$nTags - 1]\n";
    
    for($i = 0; $i < $nOTUs; $i++){
        print  "$blasted_OTUs[$i],";
        for($j = 0; $j < $nTags -1 ;$j++){
            print "$aggr_Freq[$i][$j],"
        }
        print "$aggr_Freq[$i][$nTags - 1]\n";
    }
}

######## case: $doc_match eq "single" and $doc_count eq "freq"
######################################################################
if ( $doc_match eq "single" and $doc_count eq "freq")
{

    #print header line
    print "OTUs";
    for (my $envo_term_index = 0; $envo_term_index < $nTags ;$envo_term_index++){
        print ",$tags[$envo_term_index]";
    }
    print "\n";
    
    #print each OTU line with scores for each envo term
    foreach (@blasted_OTUs){
        
        #OTU name
        my $current_otu = $_;
        print "$current_otu";
        
        my @pmid_list = @{$OTUs_hash{ $current_otu }}; #PMIDs mapped to the current OTU
        my $num_OTU_abstracts = scalar @pmid_list;
        
        #for each envo term
        for (my $envo_term_index = 0; $envo_term_index < $nTags ;$envo_term_index++){
            my $term_occurrences = 0;
            my $num_abstract_with_term = 0;
            
            foreach (@pmid_list){
                my $current_pmid = $_;
		#BUGFIX(UZI,20/07/2013): %PMID_hash doesn't contain those entries which are not found in envo text file, i.e., no environmental terms found, so unless clause is inserted
                unless ($PMID_hash{$current_pmid} eq undef) {
			my $pmid_index = $PMID_hash{$current_pmid};
                
                	if ($freq[$pmid_index][$envo_term_index] > 0){
                    		$num_abstract_with_term++
                	}
                	$term_occurrences += $freq[$pmid_index][$envo_term_index];            
            	}
	    }
            
            #print ",$term_occurrences-$num_abstract_with_term-$num_OTU_abstracts";
            
            my $term_score = $term_occurrences * ($num_abstract_with_term / $num_OTU_abstracts);
            print ",$term_score";
            
        }
        
        print "\n";
    }#end of for each blasted_otu

}


######## case: $doc_match eq "multiple" and $doc_count eq "freq"
######################################################################
if ( $doc_match eq "multiple" and $doc_count eq "freq")
{
    
    
    #print header line
    print "OTUs";
    for (my $envo_term_index = 0; $envo_term_index < $nTags ;$envo_term_index++){
        print ",$tags[$envo_term_index]";
    }
    print "\n";
    
    #print each OTU line with scores for each envo term
    foreach (@blasted_OTUs){
        
        #OTU name
        my $current_otu = $_;
        print "$current_otu";
        
        my @pmid_list = @{$OTUs_hash{ $current_otu }}; #PMIDs mapped to the current OTU
        
        #for each envo term
        for (my $envo_term_index = 0; $envo_term_index < $nTags ;$envo_term_index++){
            my $term_total_occurrences = 0;
            my $num_abstracts_with_term = 0;
            my $num_OTU_total_abstracts = 0;
            
            #for each pubmed abstract
            foreach (@pmid_list){
                my $current_pmid = $_;
		#BUGFIX(UZI,20/07/2013): %PMID_hash doesn't contain those entries which are not found in envo text file, i.e., no environmental terms found, so unless clause is inserted
                unless ($PMID_hash{$current_pmid} eq undef) {
			my $pmid_index = $PMID_hash{$current_pmid};
                
                	my $num_otu_abstract_hits = $OTU_PMID_count{$current_otu}{$current_pmid};
                
                	#count the number of abstracts linked to this OTU
                	#Alternative: can also be calculated from $OTU_PMID_count
                	$num_OTU_total_abstracts += $num_otu_abstract_hits;
                
                	#if this envo term has been found in this pubmed abstract count
                	#count the number of documents
                	if ($freq[$pmid_index][$envo_term_index] > 0){
                    	$num_abstracts_with_term += $num_otu_abstract_hits;
                	}
                
                	my $num_term_occurrences_in_this_abstract = $freq[$pmid_index][$envo_term_index];
                	$term_total_occurrences += ($num_term_occurrences_in_this_abstract * $num_otu_abstract_hits)
                }
            }
            
            #print ",$term_total_occurrences-$num_abstracts_with_term-$num_OTU_total_abstracts";
            
            my $term_score = $term_total_occurrences * ($num_abstracts_with_term / $num_OTU_total_abstracts);
            print ",$term_score";
            
        }#end of for each envo term
        print "\n";
        
    }#end of for each blasted_otu

}

######## case: $doc_match eq "multiple" and $doc_count eq "aggr"
######################################################################
if ( $doc_match eq "multiple" and $doc_count eq "aggr")
{
    
    #print header line
    print "OTUs";
    for (my $envo_term_index = 0; $envo_term_index < $nTags ;$envo_term_index++){
        print ",$tags[$envo_term_index]";
    }
    print "\n";
    
    #print each OTU line with scores for each envo term
    foreach (@blasted_OTUs){
        
        #OTU name
        my $current_otu = $_;
        print "$current_otu";
        
        my @pmid_list = @{$OTUs_hash{ $current_otu }}; #PMIDs mapped to the current OTU
        
        #for each envo term
        for (my $envo_term_index = 0; $envo_term_index < $nTags ;$envo_term_index++){
            my $term_total_occurrences = 0;
            
            #for each pubmed abstract
            foreach (@pmid_list){
                my $current_pmid = $_;
                #BUGFIX(UZI,20/07/2013): %PMID_hash doesn't contain those entries which are not found in envo text file, i.e., no environmental terms found, so unless clause is inserted
		unless ($PMID_hash{$current_pmid} eq undef) {
			my $pmid_index = $PMID_hash{$current_pmid};
                
                	my $num_otu_abstract_hits = $OTU_PMID_count{$current_otu}{$current_pmid};
                	my $num_term_occurrences_in_this_abstract = $freq[$pmid_index][$envo_term_index];
                
                	$term_total_occurrences += ($num_term_occurrences_in_this_abstract * $num_otu_abstract_hits)
                }
            }
            
            my $term_score = $term_total_occurrences;
            print ",$term_score";
            
        }#end of for each envo term
        print "\n";
        
    }#end of for each blasted_otu

}




