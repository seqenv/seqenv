#!/usr/bin/python
# ***************************************************************
# Name:      envo_blast_concat_isolation_terms.py
# Purpose:   This scripts takes a blast output file, extracts the
#         isolation terms from genbank record and appends it as the last
#         column. 
#
#            This script can filter blast files generated through the
#            the following command:
#
#            blastn -db <path_to_nt> -query <fasta_file>
#            -out <blast_file> -outfmt "6" -num_threads 8
#            -max_target_seqs 100
#
# Version:   0.1
# Authors:   Umer Zeeshan Ijaz (Umer.Ijaz@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/umer.ijaz
#            Christopher Quince (Christopher.Quince@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/christopher.quince
#             Evangelos Pafilis (vagpafilis@gmail.com)
#                  http://epafilis.info
# Created:   2012-12-12
# License:   Copyright (c) 2012 Computational Microbial Genomics Group, University of Glasgow, UK
#
#            This program is free software: you can redistribute it and/or modify
#            it under the terms of the GNU General Public License as published by
#            the Free Software Foundation, either version 3 of the License, or
#            (at your option) any later version.
#
#            This program is distributed in the hope that it will be useful,
#            but WITHOUT ANY WARRANTY; without even the implied warranty of
#            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#            GNU General Public License for more details.
#
#            You should have received a copy of the GNU General Public License
#            along with this program.  If not, see <http://www.gnu.org/licenses/>.
# **************************************************************/     

import re,urllib,sys,time,getopt
from xml.dom import minidom

def print_record(record):
    gid=[]
    params={
        'db':'nucleotide',
        'retmode':'xml',
    }
    gid=int(record.split("|")[1])
    params['id']=gid
    
    #evangelos, 28.Feb.2013
    time.sleep(0.3)
    
    url='http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?'+ urllib.urlencode(params)
    isolation_source=''
    try:
        data=urllib.urlopen(url).read()
        xmldoc=minidom.parseString(data);
        GBFeature_quals=xmldoc.getElementsByTagName('GBFeature_quals')
        if GBFeature_quals:
            for ind in range(0,xmldoc.getElementsByTagName('GBFeature_quals')[0].getElementsByTagName('GBQualifier').length-1):
                if xmldoc.getElementsByTagName('GBFeature_quals')[0].getElementsByTagName('GBQualifier')[ind].getElementsByTagName("GBQualifier_name")[0].childNodes[0].nodeValue=='isolation_source':
                    isolation_source=xmldoc.getElementsByTagName('GBFeature_quals')[0].getElementsByTagName('GBQualifier')[ind].getElementsByTagName("GBQualifier_value")[0].childNodes[0].nodeValue
            print record.rstrip('\n')+"\t"+isolation_source
        else:
            print record.rstrip('\n')+"\t"+isolation_source
    except:
        print record.rstrip('\n')+"\t"+isolation_source

def usage():
    print 'Usage:'
    print '\tpython envo_blast_concat_isolation_terms.py -b <blast_file> > <output_file>'
def main(argv):
    blast_file=''
    try:
        opts, args = getopt.getopt(argv,"hb:",["blast_file="])
    except getopt.GetoptError:
        usage()
        exit(2)
    for opt, arg in opts:
        if opt == '-h':
            usage()
            exit()
        elif opt in ("-b", "--blast_file"):
            blast_file = arg
    if blast_file=="":
        usage()
        exit()            
    
    #check if it is a single record or a file
    if re.findall(r'gi\|(\w.*?)\|',blast_file):
        print_record(blast_file)
    else:
        ins=open(blast_file,"r")    
        for line in ins:
            print_record(line)
        ins.close()

if __name__ == '__main__':
    main(sys.argv[1:])
