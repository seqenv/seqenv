#!/usr/bin/python
# ***************************************************************
# Name:      envo_get_linked_pmids.py
# Purpose:   This scripts takes a list of unique GIds
#           finds the associated pubmed id and print a assocation table
#           of the format GI \t PMID 
#           multiple PMID associated with a given GI are reported in seperate lines
#           If no pubmed id is found then then the gi will not be reported
#           
#           NB: sometimes  the eLink report a gi to pmid mapping more than once
#           duplicate lines have to be removed
#
#   TODO: the command line to generate the list of unique and sort GIs is
#   cut -f2 pit_latrine_A1000_blast_F.out | cut -f2 -d\|  | sort -un > gis_unique_sorted.tsv
#
# Version:   0.2 (based on envo_blast_concat_PMID2.py)
#
# Authors:   Umer Zeeshan Ijaz (Umer.Ijaz@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/umer.ijaz
#            Christopher Quince (Christopher.Quince@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/christopher.quince
#            Evangelos Pafilis (vagpafilis@gmail.com)
#               http://epafilis.info
# Updated:   2013-07-11
# Created:   2012-11-23
# License:   Copyright (c) 2012 Computational Microbial Genomics Group, University of Glasgow, UK
#            Copyright (c) 2012 Hellenic Center for Marine Research, Greece
#
#            This program is free software: you can redistribute it and/or modify
#            it under the terms of the GNU General Public License as published by
#            the Free Software Foundation, either version 3 of the License, or
#            (at your option) any later version.
#
#            This program is distributed in the hope that it will be useful,
#            but WITHOUT ANY WARRANTY; without even the implied warranty of
#            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#            GNU General Public License for more details.
#
#            You should have received a copy of the GNU General Public License
#            along with this program.  If not, see <http://www.gnu.org/licenses/>.
# **************************************************************/     

import re,urllib,sys,time,getopt
from xml.dom import minidom
from time import sleep

def usage():
    print 'Usage:'
    print '\tpython envo_get_linked_pmids.py -g <gid_list_input_file> -d <nucleotide/protein> <output_file>'

def main(argv):
    gid_file=''
    database='nucleotide'
    try:
        opts, args = getopt.getopt(argv,"hg:d:",["help","gid_file","database"])
    except getopt.GetoptError:
        usage()
        exit(2)
    for opt, arg in opts:
        if opt == '-h':
            usage()
            exit()
        elif opt in ("-g", "--gid_file"):
            gid_file = arg
	elif opt in ("-d", "--database"):
	    if arg=="protein":
		database="protein"
	    elif arg=="nucleotide":
		database="nucleotide"	
    if gid_file == "":
	usage()
	exit()
    
    process_gid_list(gid_file,database)



def process_gid_list(gid_file, database,email='Umer.Ijaz@glasgow.ac.uk', tool='Environmental Context'):
    
    #read the gids and prepare the ids to be sent to NCBI via eUtils
    INPUT = open(gid_file, "r")
    gids =INPUT.readlines()
    INPUT.close()
    
    gis_with_linked_pmid = {}
    
    formatted_chuncks_of_gids = []
    num_gids = len(gids)
    #read pubmed ids in chuncks
    #TODO: place batch_size in CONFIG file
    batch_size = 150
    for start_index in range(0,num_gids,batch_size):
        end_index = min(num_gids, start_index+batch_size)
         #strip off new lines and join ids with a comma
        formatted_chuncks_of_gids.append("&id=".join(gids[start_index:end_index]).replace("\n",""))
        
    #query ncbi and get the abstract text
    params = {
        "db":"pubmed",
        "tool":tool,
        "email":email,
        "retmode":"xml"
    }
    params["dbfrom"]=database

    for chunck_index in range (0, len(formatted_chuncks_of_gids)):
        url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink.fcgi?" + urllib.urlencode(params)
        url += "&id=" + formatted_chuncks_of_gids[chunck_index]
        #url to get gi - pmid link xml in batch maintaining one-to-correspondence
        #e.g.   http://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink.fcgi?db=pubmed&dbfrom=nucleotide&id=319452433&id=399935915&id=169288669
        
        #TODO: place the DELAY in CONFIG file
        DELAY = 0.3
        sleep(DELAY)
        
        data = urllib.urlopen(url).read()
        
        
        #parse XML output from eLink
        xmldoc = minidom.parseString(data)
        
        #for each linkset extract gid, and matched pmids
        #only the gis with a match are included in the XML received by NCBI
    
        linkset_records = xmldoc.getElementsByTagName('LinkSet')
        for linkset_index in range (0, len(linkset_records)):

            gid_list = linkset_records[linkset_index].getElementsByTagName('IdList')[0]
            gid_element = gid_list.getElementsByTagName('Id')
            gid = gid_element[0].firstChild.nodeValue
            
            gis_with_linked_pmid[gid] = 1
            
            records_linked_to_gi = linkset_records[linkset_index].getElementsByTagName('LinkSetDb')
            for link_index in range (0, len(records_linked_to_gi)):
                linked_db = records_linked_to_gi[link_index].getElementsByTagName('DbTo')[0].firstChild.nodeValue
                linked_entry_id = records_linked_to_gi[link_index].getElementsByTagName('Link')[0].getElementsByTagName('Id')[0].firstChild.nodeValue
                print gid+"\t"+linked_entry_id
                
            
if __name__ == '__main__':
    main(sys.argv[1:])
