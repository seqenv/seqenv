#!/usr/bin/python
# ***************************************************************
# Name:      envo_blast_concat_PMID2.py
# Purpose:   This scripts takes a blast output file and a file with mappings of gids
#        to pubmed id and appends it as the last column. If a gid does not have an
#        associated pubmed id is found then the last column will
#        have -1 as value.
#
# Version:   0.1
# Authors:   Umer Zeeshan Ijaz (Umer.Ijaz@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/umer.ijaz
#            Christopher Quince (Christopher.Quince@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/christopher.quince
#            Evangelos Pafilis (vagpafilis@gmail.com)
#                 http://epafilis.info
# Created:   2012-11-23
# License:   Copyright (c) 2012 Computational Microbial Genomics Group, University of Glasgow, UK
#            Copyright (c) 2012 Hellenic Center for Marine Research, Greece
#
#            This program is free software: you can redistribute it and/or modify
#            it under the terms of the GNU General Public License as published by
#            the Free Software Foundation, either version 3 of the License, or
#            (at your option) any later version.
#
#            This program is distributed in the hope that it will be useful,
#            but WITHOUT ANY WARRANTY; without even the implied warranty of
#            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#            GNU General Public License for more details.
#
#            You should have received a copy of the GNU General Public License
#            along with this program.  If not, see <http://www.gnu.org/licenses/>.
# **************************************************************/     

import re,sys,time,getopt

def usage():
    print 'Usage:'
    print '\tpython envo_blast_concat_PMID3.py -b <blast_file> -m <gid_pmid_map_file> > <output_file>'

def load_gid_pmid_mappings (gid_pmid_map_file):
    
    gid_pmids_map = {}
    
    INPUT = open (gid_pmid_map_file, "r")
    lines = INPUT.readlines()
    INPUT.close

    for line in lines:
        line = line.replace("\n", "")
        gid = line.split("\t")[0]
        pmid = line.split("\t")[1]
        
        #create pmid list in dictionary for gi if the list does not exist    
        try: 
            gid_pmids_map[ gid ]
        except KeyError: #equivalent of Perl's if not exists
            gid_pmids_map[ gid ] = []
        
        #append pmid to the list of pmids mapped to the gi
        gid_pmids_map [ gid ].append( pmid )
        
        #print "*:",gid,gid_pmids_map[ gid ]
        
    
    return gid_pmids_map

def print_record(record, gid_pmids_map):
    
    current_gid= record.split("|")[1]

    try: 
        pmid_list = gid_pmids_map[ current_gid ]
        
        for pmid in pmid_list:
            print record.rstrip('\n')+"\t"+ pmid
    except KeyError: #equivalent of Perl's if not exists
        print record.rstrip('\n')+"\t"+'-1'

def main(argv):
    blast_file=''
    try:
        opts, args = getopt.getopt(argv,"hb:m:",["blast_file="])
    except getopt.GetoptError:
        usage()
        exit(2)
    for opt, arg in opts:
        if opt == '-h':
            usage()
            exit()
        elif opt in ("-b", "--blast_file"):
            blast_file = arg
        elif opt in ("-m", "--map_file"):
            gid_pmid_map_file = arg
    
    if blast_file=="" or gid_pmid_map_file=="" :
        usage()
        exit()

    gid_pmids_map = load_gid_pmid_mappings( gid_pmid_map_file )
    
    #check if it is a single record or a file
    if re.findall(r'gi\|(\w.*?)\|',blast_file):
        print_record(blast_file, gid_pmids_map)
    else:
        ins=open(blast_file,"r")	
        for line in ins:
            print_record(line, gid_pmids_map)
        ins.close()

if __name__ == '__main__':
    main(sys.argv[1:])
