#!/usr/bin/perl
## ***************************************************************
# Name:      envo_extract_records.pl
# Purpose:   This scripts takes a frequency file in csv format and generates
#	     csv files for each record in the frequency file. The listing of
#	     these files is saved as envo_listing.txt. This list can then be
#	     used with envo_ids_to_terms.sh to generate the dot files for each
#	     csv file.
# Version:   0.1
# Authors:   Umer Zeeshan Ijaz (Umer.Ijaz@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/umer.ijaz
#            Christopher Quince (Christopher.Quince@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/christopher.quince
# Created:   2012-10-20
# License:   Copyright (c) 2012 Computational Microbial Genomics Group, University of Glasgow, UK
#
#            This program is free software: you can redistribute it and/or modify
#            it under the terms of the GNU General Public License as published by
#            the Free Software Foundation, either version 3 of the License, or
#            (at your option) any later version.
#
#            This program is distributed in the hope that it will be useful,
#            but WITHOUT ANY WARRANTY; without even the implied warranty of
#            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#            GNU General Public License for more details.
#
#            You should have received a copy of the GNU General Public License
#            along with this program.  If not, see <http://www.gnu.org/licenses/>.
# **************************************************************/

use strict;
use Getopt::Long;

my %opts; #store the input arguments
GetOptions(\%opts,
        'input|i=s',
        'listing|l=s',
);

if(not defined $opts{"input"})
        {
print <<EOF;
Usage:
To generate the default envo_listing.txt
        perl envo_extract_records.pl -i <input_file>
To specify your own listing
	perl envo_extract_records.pl -i <input_file> -l <listing_file>
All the csv files will be stored in the current folder with their names
as record names.
EOF
        exit;
        }

my $csv_file = $opts{"input"};

open(FILE, $csv_file) or die "Can't open $csv_file\n";
my $line = <FILE>;
chomp($line);

my @tokens = split(/,/,$line);
shift(@tokens);

my @samples  = @tokens;
my $cols=scalar(@tokens);
my @values;
my @record_names;
my ($i,$j);

while($line = <FILE>){
  chomp($line);
  @tokens = split(/,/,$line);
  push(@record_names, shift(@tokens));
  my $j = 0;
  foreach my $tok(@tokens){
    $values[$i][$j] = $tok;
    $j++;
  }
  $i++;
}
my $rows=$i;

my $listing_file="envo_listing.txt";
unless(not defined $opts{"listing"})
        {$listing_file=$opts{"listing"};}
open(OUT2,">".$listing_file);
for($i=0;$i<$rows;$i++)
	{
	open(OUT,">".$record_names[$i].".csv");
	print OUT "Samples";
	for($j=0;$j<$cols;$j++)
		{
		if ($values[$i][$j]!=0){
			print OUT ",",$samples[$j];
			}
		}
	print OUT "\n",$record_names[$i];
	print OUT2 $record_names[$i],".csv\n";
	for($j=0;$j<$cols;$j++)
		{
		if ($values[$i][$j]!=0){
			print OUT ",",$values[$i][$j];
			}
		}
	close(OUT);

	}
close(OUT2);
close(FILE);
