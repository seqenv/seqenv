#!/usr/bin/env Rscript
# ***************************************************************
# Name:      envo_gen_word_cloud.R
# Purpose:   This scripts takes the frequency file and generates
#	     a word cloud for each record in it.
# Version:   0.2
# History:  Removed the scaling bug, now it doesn't require the scaling parameters as well as minimum frequency.
# Authors:   Umer Zeeshan Ijaz (Umer.Ijaz@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/umer.ijaz
#            Christopher Quince (Christopher.Quince@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/christopher.quince
# Created:   2013-03-17
# License:   Copyright (c) 2012 Computational Microbial Genomics Group, University of Glasgow, UK
#
#            This program is free software: you can redistribute it and/or modify
#            it under the terms of the GNU General Public License as published by
#            the Free Software Foundation, either version 3 of the License, or
#            (at your option) any later version.
#
#            This program is distributed in the hope that it will be useful,
#            but WITHOUT ANY WARRANTY; without even the implied warranty of
#            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#            GNU General Public License for more details.
#
#            You should have received a copy of the GNU General Public License
#            along with this program.  If not, see <http://www.gnu.org/licenses/>.
# **************************************************************/

suppressPackageStartupMessages(library("optparse"))
suppressPackageStartupMessages(library("RColorBrewer"))
suppressPackageStartupMessages(library("wordcloud"))

#specify desired options in a list
option_list <- list(
        make_option("--ffile", action="store",default=NULL, help="ENVO frequency file"),
	make_option("--width", type="integer",default=800, help="Width of png files [default %default]"),
	make_option("--height", type="integer",default=800, help="Height of png files [default %default]"),
	make_option("--ro",action="store_true",default=FALSE, help="Flag: Random order of terms"),
	make_option("--rc",action="store_true",default=FALSE, help="Flag: Random color of terms")
)

#get command line options
opt<-parse_args(OptionParser(usage="%prog [options] file", option_list=option_list))


if(is.null(opt$ffile))
  quit()

#Importing data now
AS <-read.csv(opt$ffile,header=TRUE,row.names=1)

pal <- brewer.pal(6,"Dark2")
pal <- pal[-(1)]

for(i in seq(1,dim(AS)[1])){
  if(sum(AS[i,])>0){
  	png(filename = paste(rownames(AS)[i],".png",sep=""),width = as.numeric(opt$width), height = as.numeric(opt$height))
  	d<-data.frame(colnames(AS),t(AS[i,]))
  	colnames(d)<-c("word","freq")
  	d<-d[d[,2]>0,]
  	wordcloud(d[,1],d[,2],min.freq=0,max.words=Inf,scale=c(8,.3),random.order=opt$ro,random.color=opt$rc, rot.per=.15,colors=pal)
  	garbage<-dev.off()
  }	
}


