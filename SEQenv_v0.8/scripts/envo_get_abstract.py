#!/usr/bin/python
# ***************************************************************
# Name:      envo_get_abstract.py
# Purpose:   This scripts either takes a pubmed id or doi for a 
#        journal and downloads it's abstract
# Version:   0.2
# History:   0.1 to 0.2 includes title of the abstracts
# Authors:   Umer Zeeshan Ijaz (Umer.Ijaz@glasgow.ac.uk)
#               http://userweb.eng.gla.ac.uk/umer.ijaz
#         Christopher Quince (Christopher.Quince@glasgow.ac.uk)
#               http://userweb.eng.gla.ac.uk/christopher.quince
#         Evangelos Pafilis (vagpafilis@gmail.com)
#               http://epafilis.info
# Created:   2012-10-20
# License:   Copyright (c) 2012 Computational Microbial Genomics Group, University of Glasgow, UK
#
#        This program is free software: you can redistribute it and/or modify
#        it under the terms of the GNU General Public License as published by
#        the Free Software Foundation, either version 3 of the License, or
#        (at your option) any later version.
#
#        This program is distributed in the hope that it will be useful,
#        but WITHOUT ANY WARRANTY; without even the implied warranty of
#        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#        GNU General Public License for more details.
#
#        You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <http://www.gnu.org/licenses/>.
#**************************************************************/
     
import urllib,sys,getopt
from xml.dom import minidom
from django.utils.encoding import smart_str, smart_unicode

#evangelos, 28.Feb.2013
from time import sleep

def usage():
    print 'Usage:'
    print '\tpython envo_get_abstract.py -i <pubmed_id> > <output_file>'
    print '\t\tor'
    print '\tpython envo_get_abstract.py -d <doi> > <output_file>'
    print '\t\tor'
    print '\tpython envo_get_abstract.py -b <pubmed_id_list_input_file>'
    print 'for example,'
    print '\tpython envo_get_abstract.py -i 22972979'
    print '\tpython envo_get_abstract.py -d 10.1038/ng1946'
    print '\tpython envo_get_abstract.py -b pmids_unique_sorted.tsv'

def main(argv):
    citation=''
    pubmed_id_list_provided = False
    
    try:
        opts, args = getopt.getopt(argv,"hi:d:b:",["help","pubmed_id=","doi=","batch="])
    except getopt.GetoptError:
        usage()
        exit(2)
    for opt, arg in opts:
        if opt == '-h':
            usage()
            exit()
        elif opt in ("-i", "--pubmed_id"):
            citation = get_citation_from_pmid(arg)
        elif opt in ("-d", "--doi"):
            print arg
            exit()
            citation = get_citation_from_doi(arg)
        elif opt in ("-b", "--batch"):
            pubmed_id_list_provided = True
            process_pmid_list(arg)
        
    if citation=="" and pubmed_id_list_provided == False:
        usage()
        exit()

    if citation != "":
        for line in text_output(citation):
            print smart_str(line)

def process_pmid_list(pubmed_id_list_input_file, email='pafilis@hcmr.gr', tool='Environmental Context'):
    
    #read the pmids and prepare the ids to be sent to NCBI via eUtils
    INPUT = open(pubmed_id_list_input_file, "r")
    pmids =INPUT.readlines()
    INPUT.close()
    
    formatted_chuncks_of_pmids = []
    num_pmids = len(pmids)
    
    #read pubmed ids in chuncks
    #TODO: place batch_size in CONFIG file
    batch_size = 150
    for start_index in range(0,num_pmids,batch_size):
        end_index = min(num_pmids, start_index+batch_size)
         #strip off new lines and join ids with a comma
        formatted_chuncks_of_pmids.append(",".join(pmids[start_index:end_index]).replace("\n",""))
    
    #query ncbi and get the abstract text
    params = {
        "db":"pubmed",
        "tool":tool,
        "email":email,
        "retmode":"xml"
    }
    for chunck_index in range (0, len(formatted_chuncks_of_pmids)):
        params["id"] = formatted_chuncks_of_pmids[chunck_index]
        #url to get abstract xml e.g.  http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&retmode=xml&id=10074542,10319465,1042576,.....
        url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?" + urllib.urlencode(params)
        #print url
        
        #TODO: place the DELAY in CONFIG file
        DELAY = 0.3
        sleep(DELAY)
       
        #uzi modified this 
        data = urllib.urlopen(url).read()
	check_status=str(data)
        while (check_status.find("<h1>Service unavailable!</h1>",0,len(check_status))!=-1 or check_status.find("<h1>Bad Gateway!</h1>",0,len(check_status))!=-1):
		print "NCBI's server is busy. Trying once again!"
		data = urllib.urlopen(url).read()
		check_status=str(data)
	
	#parse XML output from PubMed
        xmldoc = minidom.parseString(data)
        
        #for each abstract extract id, title, text and write in a file named after the id in the abstracts folder
        citation_records = xmldoc.getElementsByTagName('MedlineCitation')
                
        for citation_index in range (0, len(citation_records)):
            pubmed_id = citation_records[citation_index].getElementsByTagName('PMID')[0].firstChild.nodeValue
            
            article = citation_records[citation_index].getElementsByTagName('Article')[0]
            title = article.getElementsByTagName('ArticleTitle')[0].firstChild.data
            
            text =""
            if len(article.getElementsByTagName('AbstractText')) < 1:
                #continue
                text =""
            else:
                for abstract_text_node in article.getElementsByTagName('AbstractText'):
                    text += abstract_text_node.firstChild.data + " "
            #print pubmed_id.encode("utf-8"), title.encode("utf-8"), "\n", text.encode("utf-8")
            ABSTRACT_OUTPUT = open ("./documents/"+pubmed_id+".txt", "w")
            ABSTRACT_OUTPUT.write( title.encode("utf-8") + "\n" + text.encode("utf-8"))
            ABSTRACT_OUTPUT.close()


def get_citation_from_doi(query, email='YOUR EMAIL GOES HERE', tool='doi tool', database='pubmed'):
    params = {
        'db':database,
        'tool':tool,
        'email':email,
        'term':query,
        'usehistory':'y',
        'retmax':1
    }
       
    # try to resolve the PubMed ID of the DOI
    url = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?' + urllib.urlencode(params)
    
    #evangelos, 28.Feb.2013
    sleep(0.3) 
    
    data = urllib.urlopen(url).read()
       
    # parse XML output from PubMed...
    xmldoc = minidom.parseString(data)
    ids = xmldoc.getElementsByTagName('Id')
       
    # nothing found, exit
    if len(ids) == 0:
        exit(0)
        #raise Exception("DoiNotFound")
     
    # get ID
    id = ids[0].childNodes[0].data

    # remove unwanted parameters
    params.pop('term')
    params.pop('usehistory')
    params.pop('retmax')

    # and add new ones...
    params['id'] = id
     
    params['retmode'] = 'xml'
     
    # get citation info:
    url = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?' + urllib.urlencode(params)
    #evangelos, 28.Feb.2013
    sleep(0.3)
    data = urllib.urlopen(url).read()
     
    return data

def get_citation_from_pmid(query, email='YOUR EMAIL GOES HERE', tool='pubmed id tool', database='pubmed'):
    params = {
        'db':database,
        'tool':tool,
        'email':email,
    }
       
    params['id'] = query
     
    params['retmode'] = 'xml'
    # get citation info:
    url = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?' + urllib.urlencode(params)
    #evangelos, 28.Feb.2013
    #The previous URL can be simplified into:
    #http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&retmode=xml&id=10074542,10319465,1042576,.....
    sleep(0.3)
    print url
    
    data = urllib.urlopen(url).read()
    # parse XML output from PubMed...
    xmldoc = minidom.parseString(data)
    
    #check if the record exists
    if len(xmldoc.getElementsByTagName('PubmedArticle'))!=1:
        exit(0)
    return data


def text_output(xml):
    """Makes a simple text output from the XML returned from efetch"""
     
    xmldoc = minidom.parseString(xml)
     

    if len(xmldoc.getElementsByTagName('AbstractText'))!=1:
        exit(0)
 
    abstract = xmldoc.getElementsByTagName('AbstractText')[0]
    abstract = abstract.childNodes[0].data
            
    title = xmldoc.getElementsByTagName('ArticleTitle')[0]
    title = title.childNodes[0].data
            
    output = []
    output.append(title)
    output.append(abstract)
    return output

     
if __name__ == '__main__':
    main(sys.argv[1:])
