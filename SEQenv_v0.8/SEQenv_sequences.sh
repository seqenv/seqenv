#!/bin/bash
# ***************************************************************
# Name:    SEQenv_sequences.sh
# Purpose: Pipeline to link nucleotide/protein sequences to environmental descriptive terms
# Dependencies:  
# 	Rscripts
#    		Install in R:     
#	     		install.packages("optparse")
#            		install.packages("wordcloud")
#            		install.packages("RColorBrewer")
#            		install.packages("optparse")
#            		install.packages("gplots")
# 	Perlscripts
#    		Package: ONTO-PERL
#    		Install: perl ‐MCPAN -e shell install Bundle::CPAN
#                    			      install OBO::Core::Ontology
# 	Graphviz
#    		Install: http://www.graphviz.org/Download.php
# 	GNU Parallel 
#    		Install: http://www.gnu.org/software/parallel/
# 	blastn/blastp
#    		Software: http://www.ncbi.nlm.nih.gov/books/NBK1763/
#    		nt/nr database, go to your local blastn db folder and use update_blastdb.pl: 
#        		update_blastdb.pl --showall
#        		update_blastdb.pl --decompress nt/nr
#	SEQenv_tagger
#		Software is provided in the installation folder
#		Run "make" to compile it
#		Prerequisite: boost library along with it's developmental version
#		For example, in CentOS, you can run the following command:
#			sudo yum install boost boost-devel 
# Version:   0.8
# Authors:   Umer Zeeshan Ijaz (Umer.Ijaz@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/umer.ijaz/index.htm
#            Evangelos Pafilis (vagpafilis@gmail.com)
#                  http://epafilis.info
# Acknowledgements:
#	     This work is made possible through the following hackathons supported by European Union's 
#	     Earth System Science and Environmental Management ES1103 COST Action ("Microbial ecology & 
#	     the earth system: collaborating for insight and success with the new generation of sequencing tools"):
#			(1) From Signals to Environmentally Tagged Sequences" (Ref: ECOST-MEETING-ES1103-050912-018418)
#			    September 27th-29th 2012, Hellenic Centre for Marine Research, Crete, Greece
#			(2) From Signals to Environmentally Tagged Sequences II" (Ref: ECOST-MEETING-ES1103-100613-031037)
#			    June 10th-13th 2013, Hellenic Centre for Marine Research, Crete, Greece
#	     This software is made possible through the useful discussions between the following participants of the above 
#	     mentioned hackathons:
#			Chris Quince (cq8u@udcf.gla.ac.uk) 
#			Umer Zeeshan Ijaz (Umer.Ijaz@glasgow.ac.uk) 
#			Evangelos Pafilis (pafilis@hcmr.gr) 
#			Christina Pavloudi (cpavloud@hcmr.gr) 
#			Anastasis Oulas (oulas@hcmr.gr) 
#			Julia Schnetzer (jschnetz@mpi-bremen.de) 
#			Aaron Weimann (aaron.weimann@uni-duesseldorf.de) 
#			Alica Chronakova (alicach@upb.cas.cz) 
#			Ali Zeeshan Ijaz  (alizeeshanijaz@gmail.com) 
#			Simon Berger (simon.berger@h-its.org)			
# Last modified:   2013-07-17
# License:   Copyright (c) 2013 Computational Microbial Genomics Group, University of Glasgow, UK
#
#            This program is free software: you can redistribute it and/or modify
#            it under the terms of the GNU General Public License as published by
#            the Free Software Foundation, either version 3 of the License, or
#            (at your option) any later version.
#
#            This program is distributed in the hope that it will be useful,
#            but WITHOUT ANY WARRANTY; without even the implied warranty of
#            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#            GNU General Public License for more details.
#
#            You should have received a copy of the GNU General Public License
#            along with this program.  If not, see <http://www.gnu.org/licenses/>.
# **************************************************************/     

HELPDOC=$( cat <<EOF
SEQenv -sequences- Pipeline to link nucleotide/protein sequences to environmental descriptive terms

Usage:
    bash `basename $0` -f <fasta_file.fasta> [options]
Options:
    -t Text source (1: GenBank record "isolation source" field, 2: PubMed abstracts (Default: 1))

    -p Parallelize using GNU Parallel
    -c Number of cores to use (Default: 10)

    -m Minimum percentage identity in blastn/blastp (Default: 97)
    -q Minimum query coverage in blastn/blastp (Default: 97)
    -r Number of reference matches (Default: 10)

    -s Sequence type (nucleotide/protein) (Default: nucloetide)
    -d Extract terms for the given ENVO ID (Default: all)
        all=Consider all terms
        Examples:
        ENVO:00010483=Environmental Material                    
        ENVO:00002297=Environmental Features
        ENVO:00000428=Biome
        ENVO:00002036=Habitat

EOF
)

set -o errexit

# = Parameters to set ============== #
LOGFILE="`pwd`/SEQenv.log" # Where to save the log
BLAST_DIR="/home/opt/ncbi-blast-2.2.28+/bin"; # Path where blastn/blastp is installed
BLASTDB_DIR="/home/opt/ncbi-blast-2.2.28+/db"; # Path where nt/nr is installed
FASTA_FILE=""   # This field should be empty
PARALLELIZE_FLAG=0
NUMBER_OF_CORES=10
NUMBER_OF_REFERENCE_MATCHES=100
MINIMUM_PERCENT_IDENT=97
MINIMUM_QUERY_COVERAGE=97
ENVO_TERM="all"
SEQUENCE_TYPE="nucleotide"
TEXT_SOURCE=1

#document matching and counting for envo score calcuation
DOCUMENT_MATCH="single" # Possible values:multiple,single
DOCUMENT_COUNT="aggr" # Possible values: aggr (aggregative), freq (document frequency weight applies) [default]
NORMALIZATION="divrowsum" # Possible values: none, zscore, divrowsum, zscore

# =/Parameters to set ============== #


CURRENT_DIR=`pwd`

# = Enable FP support ============== #
# By default, there is limited capability in bash to handle floating point
# operations. In this script bc is used to calculate the floating point operations.
# $float_scale parameter specifies the precision of the floating point.
# Reference: http://www.linuxjournal.com/content/floating-point-math-bash

float_scale=5
# Evaluate a floating point number expression.

function float_eval()
{
    local stat=0
    local result=0.0
    if [[ $# -gt 0 ]]; then
        result=$(echo "scale=$float_scale; $*" | bc -q 2>/dev/null)
        stat=$?
        if [[ $stat -eq 0  &&  -z "$result" ]]; then stat=1; fi
    fi
    echo $result
    return $stat
}


# Evaluate a floating point number conditional expression.

function float_cond()
{
    local cond=0
    if [[ $# -gt 0 ]]; then
        cond=$(echo "$*" | bc -q 2>/dev/null)
        if [[ -z "$cond" ]]; then cond=0; fi
        if [[ "$cond" != 0  &&  "$cond" != 1 ]]; then cond=0; fi
    fi
    local stat=$((cond == 0))
    return $stat
}

function ceil () {
  echo "define ceil (x) {if (x<0) {return x/1} \
        else {if (scale(x)==0) {return x} \
        else {return x/1 + 1 }}} ; ceil($1)" | bc;
 }

# =/Enable FP support ============== #


# Create directories if they don't exist yet
function create_dirs() {
    local dir
    for dir in "$@"
    do
        if [ ! -d "$dir" ]; then
            mkdir "$dir"
        fi
    done
}

# Check if files exist
function check_prog() {
    local prog
    for prog in "$@"
    do
    if which $prog >/dev/null; then
        SEQenv_print 'Using ' $prog
    else
        echo "$prog not in your path" >&2; exit 1;
    fi

    done
}

function skip_gen_file(){
    if [ -f "$1" ]; then
    echo "true"
    else
    echo "false"
    fi
}

function skip_gen_dir(){
    if [ -d "$1" ]; then
        echo "true"
    else
        echo "false"
    fi
}

function SEQenv_print() {
    echo [`date "+%Y-%m-%d %H:%M:%S"`] "$@" | tee -a $LOGFILE
}


# Parse options
while getopts ":phc:r:s:t:f:m:q:d:" opt; do
    case $opt in
        p)
            PARALLELIZE_FLAG=1
            ;;
        f)
            FASTA_FILE=$OPTARG
            ;;
	s)
	    SEQUENCE_TYPE=$OPTARG
	    ;;
        t)
            TEXT_SOURCE=$OPTARG
            ;;
        c)
            NUMBER_OF_CORES=$OPTARG
            ;;
	d)
	    ENVO_TERM=$OPTARG
	    ;;
        r)
            NUMBER_OF_REFERENCE_MATCHES=$OPTARG
            ;;
        m)
            MINIMUM_PERCENT_IDENT=$OPTARG
	    ;;
        q)
            MINIMUM_QUERY_COVERAGE=$OPTARG
            ;;
	h)
            echo "$HELPDOC"
            exit 0
            ;;
        \?)
            echo "$HELPDOC"
            echo "Invalid option: -$OPTARG" >&2
            exit 1
            ;;
    esac
done
if [ -z $FASTA_FILE ] 
then
        echo "$HELPDOC"
        exit 1
fi

if [ $TEXT_SOURCE -gt 2 ] || [ $TEXT_SOURCE -lt 1 ]
then
        echo "$HELPDOC"
        exit 1
fi

if [ "$SEQUENCE_TYPE" != "nucleotide" ] && [ "$SEQUENCE_TYPE" != "protein" ]
then
	echo "$HELPDOC"
	exit 1
fi


SEQenv_print SEQenv -sequences- v0.8

check_prog $BLAST_DIR/blastn
check_prog $BLAST_DIR/blastp

if [ $PARALLELIZE_FLAG -eq 1 ]
then
    check_prog parallel
fi

# Using /usr/bin/dirname to get the full path to this script location
# without being affected from where this script was invoked
export SEQENV_PIPELINE_DIR=$(cd "$(dirname "$0")"; pwd)

check_prog $SEQENV_PIPELINE_DIR/SEQenv_tagger/seqenv


if [[ -n $SEQENV_PIPELINE_DIR ]];
then
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_parse_environments.sh
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_blast_concat_PMID3.py
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_get_linked_pmids.py
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_extract_ids_level.pl  
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_gen_word_cloud.R  
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo.obo
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_extract_records.pl    
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_gen_heapmap.R        
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_get_abstract.py   
    check_prog $SEQENV_PIPELINE_DIR/scripts/fastagrep.pl
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_blast_concat_isolation_terms.py  
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_filter_blast.pl       
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_gen_OTUs_freq.pl
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_gen_samples_freq.R
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_process_weight_matrix.R
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_extract_abundant_OTUs.R
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_gen_dot_files.pl      
    check_prog $SEQENV_PIPELINE_DIR/scripts/envo_ids_to_terms.pl
else
    SEQenv_print "SEQENV_PIPELINE_DIR is not set. Unable to locate scripts directory!"
    exit 1;
fi

fileName=`echo "$(basename $FASTA_FILE)" | cut -d'.' -f1`

#Generate fasta map
SEQenv_print "STEP 1: Generate mappings for sequence headers in FASTA file."
mapFileName=$fileName'_M'
if [ "$(skip_gen_file $mapFileName'.fa')" == "true" ];then
   SEQenv_print $mapFileName'.fa' already exists! Skipping this step.
else
   awk '/^>/{gsub("^>","",$0);print "C"(++i)"\t"$0}' < $FASTA_FILE > $mapFileName'.map'
   awk '/^>/{$0=">C"(++i)}1' < $FASTA_FILE > $mapFileName'.fa'
   SEQenv_print $mapFileName'.map' and $mapFileName'.fa' are successfully generated. 
fi



# Run blastn/blastp on $mapFileName'.fa'
if [ "$SEQUENCE_TYPE" == "nucleotide" ]; then
	SEQenv_print "STEP 2: Blast against NCBI's nt database with minimum percentage identity of $MINIMUM_PERCENT_IDENT%, maximum of $NUMBER_OF_REFERENCE_MATCHES reference sequences, and evalue of 0.0001 in blastn."
elif [ "$SEQUENCE_TYPE" == "protein" ]; then
	SEQenv_print "STEP 2: Blast against NCBI's nr database with maximum of $NUMBER_OF_REFERENCE_MATCHES reference sequences, and evalue of 0.0001 in blastp."
fi

# Format for blastn/blastp
blastOutFmt="\"6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs staxids\""
blastFileName=$mapFileName'_blast'
if [ "$(skip_gen_file $blastFileName'.out')" == "true" ];then
   SEQenv_print $blastFileName'.out' already exists! Skipping this step.
elif [ $PARALLELIZE_FLAG -eq 1 ]; then

    # Get the file size in KB
    sizeFileBytes=$(du -b ${mapFileName}'.fa' | sed 's/\([0-9]*\)\(.*\)/\1/')
    sizeChunks=$(ceil $(float_eval "$sizeFileBytes / ($NUMBER_OF_CORES * 1024)"))
    sizeChunksString="${sizeChunks}k"
    startTime=`date +%s`

    if [ "$SEQUENCE_TYPE" == "nucleotide" ]; then
    	cat $mapFileName'.fa' | parallel --block $sizeChunksString --recstart '>' --pipe $BLAST_DIR/blastn -perc_identity $MINIMUM_PERCENT_IDENT -evalue 0.00001 -dust no -num_threads 1 -outfmt $blastOutFmt -max_target_seqs $NUMBER_OF_REFERENCE_MATCHES -db $BLASTDB_DIR'/nt' -query - > $blastFileName'.out'
	SEQenv_print "blastn using GNU parallel took $(expr `date +%s` - $startTime) seconds to generate $blastFileName'.out' from $mapFileName.fa."
    elif [ "$SEQUENCE_TYPE" == "protein" ]; then		
    	cat $mapFileName'.fa' | parallel --block $sizeChunksString --recstart '>' --pipe $BLAST_DIR/blastp  -evalue 0.00001 -num_threads 1 -outfmt $blastOutFmt -max_target_seqs $NUMBER_OF_REFERENCE_MATCHES -db $BLASTDB_DIR'/nr' -query - > $blastFileName'.out'
	SEQenv_print "blastp using GNU parallel took $(expr `date +%s` - $startTime) seconds to generate $blastFileName'.out' from $mapFileName.fa."
    fi	
else
    startTime=`date +%s`
    if [ "$SEQUENCE_TYPE" == "nucleotide" ]; then
	$BLAST_DIR/blastn -db  $BLASTDB_DIR'/nt' -query $mapFileName'.fa' -perc_identity $MINIMUM_PERCENT_IDENT -out $blastFileName'.out' -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs staxids" -max_target_seqs $NUMBER_OF_REFERENCE_MATCHES -evalue 0.00001 -dust no -num_threads 1
    SEQenv_print "blastn took $(expr `date +%s` - $startTime) seconds to generate $blastFileName'.out' from $mapFileName.fa."
    elif [ "$SEQUENCE_TYPE" == "protein" ]; then 
	$BLAST_DIR/blastp -db  $BLASTDB_DIR'/nr' -query $mapFileName'.fa'  -out $blastFileName'.out' -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs staxids" -max_target_seqs $NUMBER_OF_REFERENCE_MATCHES -evalue 0.00001 -num_threads 1
    SEQenv_print "blastp took $(expr `date +%s` - $startTime) seconds to generate $blastFileName'.out' from $mapFileName.fa."
    fi	
fi

# Filter out low quality hits
if [ "$SEQUENCE_TYPE" == "nucleotide" ]; then
	SEQenv_print "STEP 3: Filter out low quality hits with query coverage < $MINIMUM_QUERY_COVERAGE%."
elif [ "$SEQUENCE_TYPE" == "protein" ]; then
        SEQenv_print "STEP 3: Filter out low quality hits with query coverage < $MINIMUM_QUERY_COVERAGE% and percentage identity < $MINIMUM_PERCENT_IDENT%."
fi
blastFilteredFileName=$blastFileName'_F'
if [ "$(skip_gen_file $blastFilteredFileName'.out')" == "true" ];then
   SEQenv_print $blastFilteredFileName'.out' already exists! Skipping this step.
else
	if [ "$SEQUENCE_TYPE" == "nucleotide" ]; then
		cat $blastFileName'.out' | awk -F"\t" -v minimum_query_coverage=$MINIMUM_QUERY_COVERAGE '$13>=minimum_query_coverage{print $0}' > $blastFilteredFileName'.out'
	elif [ "$SEQUENCE_TYPE" == "protein" ]; then
		cat $blastFileName'.out' | awk -F"\t" -v minimum_query_coverage=$MINIMUM_QUERY_COVERAGE -v minimum_ident=$MINIMUM_PERCENT_IDENT '($13>=minimum_query_coverage && $3>=minimum_ident){print $0}' > $blastFilteredFileName'.out'
	fi
   SEQenv_print $blastFilteredFileName'.out' is successfully generated.
fi


# Retrieve the selected type of documents from NCBI
SEQenv_print "STEP 4: Download data from NCBI."

if [ "$(skip_gen_dir documents)" == "true" ];then
       SEQenv_print Folder documents already exists! Skipping this step.
else
    create_dirs documents

    #get the GIs list (unique, sorted)
    cut -f2 $blastFilteredFileName'.out' | cut -f2 -d\|  | sort -u > gis_unique_sorted.tsv

    #switch on text source
    if [ $TEXT_SOURCE -eq 1 ];then
        python $SEQENV_PIPELINE_DIR/scripts/envo_get_isolation_source.py -g gis_unique_sorted.tsv  > gi_isid.unsorted.tsv
        sort -un gi_isid.unsorted.tsv > gi_isid_unique_sorted.tsv
        SEQenv_print "Retrieved GenBank -isolation source- field linked to GIs. Unique entries are saved in the documents folder."

    elif [ $TEXT_SOURCE -eq 2 ];then

        #get PMID linked to the GIs
        SEQenv_print "Retrieving PMIDs linked to GIs from NCBI."
        python $SEQENV_PIPELINE_DIR/scripts/envo_get_linked_pmids.py -g gis_unique_sorted.tsv -d $SEQUENCE_TYPE  > gi_pmid.unsorted.tsv
        sort -un gi_pmid.unsorted.tsv > gi_pmid_unique_sorted.tsv

        # Download abstracts
        cut -f 2 gi_pmid_unique_sorted.tsv | sort -un > pmids_unique_sorted.tsv
        python $SEQENV_PIPELINE_DIR/scripts/envo_get_abstract.py -b pmids_unique_sorted.tsv
	SEQenv_print "Retrieved PubMed abstracts. Entries are saved in the documents folder."

    else
        echo Future case 3: use both
    fi #end-of-text-source switch

fi # end-of-generate abstracts if it does not exit

if [ $TEXT_SOURCE -eq 1 ];then
        SEQenv_print "STEP 5: Concatenate GenBank -isolation source- field IDs to blast file."
elif [ $TEXT_SOURCE -eq 2 ];then
        SEQenv_print "STEP 5: Concatenate PubMed IDs to blast file."
fi

blastPMIDFileName=$blastFilteredFileName'_PMID'

if [ "$(skip_gen_file $blastPMIDFileName'.out')" == "true" ];then
    SEQenv_print $blastPMIDFileName'.out' already exists! Skipping this step.
else
    if [ $TEXT_SOURCE -eq 1 ];then
        python $SEQENV_PIPELINE_DIR/scripts/envo_blast_concat_PMID3.py -b $blastFilteredFileName'.out' -m gi_isid_unique_sorted.tsv > $blastPMIDFileName'.out'
	rm gi_isid_unique_sorted.tsv
	rm gi_isid.unsorted.tsv
	rm gis_unique_sorted.tsv
    elif [ $TEXT_SOURCE -eq 2 ];then
        python $SEQENV_PIPELINE_DIR/scripts/envo_blast_concat_PMID3.py -b $blastFilteredFileName'.out' -m gi_pmid_unique_sorted.tsv > $blastPMIDFileName'.out'
	rm gi_pmid_unique_sorted.tsv
	rm gi_pmid.unsorted.tsv
	rm gis_unique_sorted.tsv
	rm pmids_unique_sorted.tsv
    else
        echo Future case 3: use both
    fi
    SEQenv_print $blastPMIDFileName'.out' is successfully generated.
fi

# Generate ENVO hit files
SEQenv_print "STEP 6: Run SEQenv_tagger on the documents folder and generate ENVO hits file."
envoHitsFileName=$blastFilteredFileName'_ENVO'
if [ "$(skip_gen_file $envoHitsFileName'.txt')" == "true" ];then
   SEQenv_print $envoHitsFileName'.txt' already exists! Skipping this step.
else
  cd $SEQENV_PIPELINE_DIR/SEQenv_tagger/
  ./seqenv $CURRENT_DIR/documents > $CURRENT_DIR/$envoHitsFileName'.txt'
  cd $CURRENT_DIR
  SEQenv_print $envoHitsFileName'.txt' is successfully generated.
fi

# Generate world cloud for overall community profile
SEQenv_print "STEP 7: Generate word cloud for overall community profile."
overallSampleFile=$envoHitsFileName'_overall'
if [ "$(skip_gen_file $overallSampleFile'_labels.png')" == "true" ];then
        SEQenv_print $overallSampleFile'_labels.png' already exists! Skipping this step.
else
        bash $SEQENV_PIPELINE_DIR/scripts/envo_parse_environments.sh -f $envoHitsFileName'.txt' -t $overallSampleFile'_labels' > $overallSampleFile'.csv'
        perl $SEQENV_PIPELINE_DIR/scripts/envo_ids_to_terms.pl -i $overallSampleFile'.csv' -o $SEQENV_PIPELINE_DIR/scripts/envo.obo > $overallSampleFile'_labels.csv' 2>/dev/null
        Rscript $SEQENV_PIPELINE_DIR/scripts/envo_gen_word_cloud.R  --ffile=$overallSampleFile'_labels.csv' --width=1600 --height=1600 2>/dev/null
        SEQenv_print $overallSampleFile'_labels.png' is successfully generated.
fi


# Generate frequency tables for sequences
SEQenv_print "STEP 8: Generate frequency tables for sequences."
sequencesFileName=$envoHitsFileName'_sequences'
if [ "$(skip_gen_file $sequencesFileName'.csv')" == "true" ];then
   SEQenv_print $sequencesFileName'.csv' already exists! Skipping this step.
else
   #generate the frequency table
   perl $SEQENV_PIPELINE_DIR/scripts/envo_gen_OTUs_freq.pl -e $envoHitsFileName'.txt' -b $blastPMIDFileName'.out' -m $DOCUMENT_MATCH -c $DOCUMENT_COUNT > $sequencesFileName'.csv' 
   SEQenv_print $sequencesFileName'.csv' is successfully using $DOCUMENT_MATCH document matching and $DOCUMENT_COUNT document counting.
   
   #normalize the frequency table (if selected)
   if [ $NORMALIZATION == "none" ];then
      SEQenv_print $sequencesFileName'.csv' is not normalized!
   else
       Rscript $SEQENV_PIPELINE_DIR/scripts/envo_process_weight_matrix.R  --mfile=$CURRENT_DIR/$sequencesFileName'.csv' --nfunction=$NORMALIZATION --ofile=normalized_matrix_temp.csv --opath=$CURRENT_DIR/
       
       mv normalized_matrix_temp.csv $sequencesFileName'.csv'
       SEQenv_print $NORMALIZATION normalization applied to $sequencesFileName'.csv' successfully!
   fi
fi

# Generate dot files for frequency tables of sequences
SEQenv_print "STEP 9: Generate dot files for frequency tables of sequences."
if [ "$(skip_gen_dir sequences_dot)" == "true" ];then
   SEQenv_print Folder sequences_dot already exists! Skipping this step.
else
        create_dirs sequences_dot
    cd sequences_dot
    perl $SEQENV_PIPELINE_DIR/scripts/envo_extract_records.pl  -i ../$sequencesFileName'.csv' -l sequences_list.txt
    #get unique ids from the file
    uniqueIDs=$(cat sequences_list.txt)
    for id in ${uniqueIDs[@]}
    do
            #remove
            SEQenv_print "Processing $id"
            perl $SEQENV_PIPELINE_DIR/scripts/envo_gen_dot_files.pl -i "$id" -o $SEQENV_PIPELINE_DIR/scripts/envo.obo 2>/dev/null
    done
    rm sequences_list.txt
        cd ..
        SEQenv_print Folder sequences_dot is successfully generated.
fi


# Generate labels for frequency tables of sequences
sequencesLabelFileName=$sequencesFileName'_labels'
SEQenv_print "STEP 10: Generate labels for frequency tables of sequences ($ENVO_TERM)."
if [ "$(skip_gen_file $sequencesLabelFileName'.csv')" == "true" ];then
        SEQenv_print $sequencesLabelFileName'.csv' already exists. Skipping this step.
else
        if [ "$ENVO_TERM" == "all" ];then
		perl $SEQENV_PIPELINE_DIR/scripts/envo_ids_to_terms.pl -i $sequencesFileName'.csv' -o $SEQENV_PIPELINE_DIR/scripts/envo.obo  > $sequencesLabelFileName'.csv' 2>/dev/null
        else
		perl $SEQENV_PIPELINE_DIR/scripts/envo_ids_to_terms.pl -i $sequencesFileName'.csv' -o $SEQENV_PIPELINE_DIR/scripts/envo.obo -d $ENVO_TERM > $sequencesLabelFileName'.csv' 2>/dev/null
	fi
	SEQenv_print $sequencesLabelFileName'.csv' is successfully generated.
fi



# Generate world clouds for sequences
SEQenv_print "STEP 11: Generate word clouds for sequences ($ENVO_TERM)."
if [ "$(awk 'BEGIN {FS=","} END {print NF}' $sequencesLabelFileName'.csv')" -gt 4 ]; then
    if [ "$(skip_gen_dir sequences_wc)" == "true" ];then
           SEQenv_print Folder sequences_wc already exists! Skipping this step.
    else
            create_dirs sequences_wc
            cd sequences_wc
            Rscript $SEQENV_PIPELINE_DIR/scripts/envo_gen_word_cloud.R  --ffile=$CURRENT_DIR/$sequencesLabelFileName'.csv' --width=2000 --height=2000
            cd ..
            SEQenv_print Folder sequences_wc is successfully generated.
    fi
else
        SEQenv_print Folder sequences_wc is not generated as not enough columns in $sequencesLabelFileName'.csv'.
fi


# Generate heap maps for sequences
SEQenv_print "Generate heapmap for sequences ($ENVO_TERM)."
if [ "$(skip_gen_dir sequences_heapmaps)" == "true" ];then
   SEQenv_print Folder sequences_heapmaps already exists! Skipping this step.
else
        create_dirs sequences_heapmaps
        cd sequences_heapmaps
    if [ "$(awk 'BEGIN {FS=","} END {print NF}' $CURRENT_DIR/$sequencesLabelFileName'.csv')" -gt 3 ]; then
            Rscript $SEQENV_PIPELINE_DIR/scripts/envo_gen_heapmap.R --ffile=$CURRENT_DIR/$sequencesLabelFileName'.csv' --oname=$sequencesLabelFileName --rowSize=2 --colSize=2 --width=3600 --height=6000
            SEQenv_print $sequencesLabelFileName'.png' is successfully generated.
    else
        SEQenv_print $sequencesLabelFileName'.png' not generated as not enough columns in $sequencesLabelFileName'.csv'.
    fi
    cd ..
        SEQenv_print Folder samples_heapmaps is successfully generated.
fi

SEQenv_print "Finished processing!"

